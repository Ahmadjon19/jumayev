<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main.talim');
})->name('main.page');

Route::get('main/iframe/{file}', [\App\Http\Controllers\Admin\SaytController::class, 'iframe'])->name('iframe');
Route::get('main/sayt_haqida', [\App\Http\Controllers\Admin\SaytController::class, 'sayt_haqida'])->name('sayt_haqida');
Route::get('main/muallif_haqida', [\App\Http\Controllers\Admin\SaytController::class, 'muallif_haqida'])->name('muallif_haqida');
Route::get('main/talim', [\App\Http\Controllers\Admin\SaytController::class, 'talim'])->name('talim');
Route::get('main/ilova', [\App\Http\Controllers\Admin\SaytController::class, 'ilova'])->name('ilova');
Route::get('main/form', [\App\Http\Controllers\Admin\SaytController::class, 'form'])->name('form');
Route::get('main/farmon', [\App\Http\Controllers\Admin\FarmonController::class, 'view_farmon'])->name('view.farmon');
Route::get('main/hujjat', [\App\Http\Controllers\Admin\HujjatController::class, 'view_hujjat'])->name('view.hujjat');
Route::get('main/tadqiqot', [\App\Http\Controllers\Admin\IlmiyController::class, 'view_tadqiqot'])->name('view.tadqiqot');
Route::get('main/maqola', [\App\Http\Controllers\Admin\IlmiyController::class, 'view_maqola'])->name('view.maqola');
Route::get('main/lugat', [\App\Http\Controllers\Admin\LugatController::class, 'view_lugat'])->name('view.lugat');
Route::get('main/konferensiya', [\App\Http\Controllers\Admin\KonferensiyaController::class, 'view_konferensiya'])->name('view.konferensiya');
Route::get('main/akademik', [\App\Http\Controllers\Admin\AkademikController::class, 'view_akademik'])->name('view.akademik');
Route::get('main/katalog', [\App\Http\Controllers\Admin\KatalogController::class, 'view_katalog'])->name('view.katalog');
Route::get('main/siyosat_kirish', [\App\Http\Controllers\Admin\Siyosat_kirishController::class, 'view_siyosat_kirish'])->name('view.siyosat_kirish');
Route::get('main/siyosat_metodika', [\App\Http\Controllers\Admin\Siyosat_metodikaController::class, 'view_siyosat_metodika'])->name('view.siyosat_metodika');
Route::get('main/siyosat_fanlar', [\App\Http\Controllers\Admin\Siyosat_fanlarController::class, 'view_siyosat_fanlar'])->name('view.siyosat_fanlar');
Route::get('main/savols', [\App\Http\Controllers\Admin\SavolController::class, 'view_savols'])->name('view.savols');
Route::get('main/kutubxona', [\App\Http\Controllers\Admin\KutubxonaController::class, 'view_kutubxona'])->name('view.kutubxona');
Route::get('main/havola', [\App\Http\Controllers\Admin\HavolaController::class, 'view_havola'])->name('view.havola');
Route::get('main/fotogalareya', [\App\Http\Controllers\Admin\FotogalareyaController::class, 'view_fotogalareya'])->name('view.fotogalareya');
Route::get('main/elon', [\App\Http\Controllers\Admin\ElonController::class, 'view_elon'])->name('view.elon');
Route::get('main/yangilik', [\App\Http\Controllers\Admin\YangilikController::class, 'view_yangilik'])->name('view.yangilik');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['prefix'=>'dashboard', 'middleware'=>['auth']], function ()
{
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'adminpanel'])->name('adminpanel');
    Route::get('/setting', [\App\Http\Controllers\HomeController::class, 'edit'])->name('dashboard.setting');
    Route::put('/update/{user}', [\App\Http\Controllers\HomeController::class, 'update'])->name('dashboard.update');
    Route::get('/sayt/create', [\App\Http\Controllers\Admin\SaytController::class, 'create_sayt'])->name('create.sayt');
    Route::post('/sayt/store', [\App\Http\Controllers\Admin\SaytController::class, 'store_sayt'])->name('store.sayt');
    Route::get('/sayt/view', [\App\Http\Controllers\Admin\SaytController::class, 'view_sayt'])->name('view.sayt');
    Route::get('/sayt/edit', [\App\Http\Controllers\Admin\SaytController::class, 'edit_sayt'])->name('edit.sayt');
    Route::post('/sayt/update', [\App\Http\Controllers\Admin\SaytController::class, 'update_sayt'])->name('update.sayt');

    Route::get('/muallif/create', [\App\Http\Controllers\Admin\SaytController::class, 'create_muallif'])->name('create.muallif');
    Route::post('/muallif/store', [\App\Http\Controllers\Admin\SaytController::class, 'store_muallif'])->name('store.muallif');
    Route::get('/muallif/view', [\App\Http\Controllers\Admin\SaytController::class, 'view_muallif'])->name('view.muallif');
    Route::get('/muallif/edit', [\App\Http\Controllers\Admin\SaytController::class, 'edit_muallif'])->name('edit.muallif');
    Route::post('/muallif/update', [\App\Http\Controllers\Admin\SaytController::class, 'update_muallif'])->name('update.muallif');

    Route::resource('/farmon', \App\Http\Controllers\Admin\FarmonController::class);
    Route::resource('/hujjat', \App\Http\Controllers\Admin\HujjatController::class);
    Route::resource('/ilmiy', \App\Http\Controllers\Admin\IlmiyController::class);
    Route::resource('/lugat', \App\Http\Controllers\Admin\LugatController::class);
    Route::resource('/konferensiya', \App\Http\Controllers\Admin\KonferensiyaController::class);
    Route::resource('/akademik', \App\Http\Controllers\Admin\AkademikController::class);
    Route::resource('/katalog', \App\Http\Controllers\Admin\KatalogController::class);
    Route::resource('/siyosat_kirish', \App\Http\Controllers\Admin\Siyosat_kirishController::class);
    Route::resource('/siyosat_metodika', \App\Http\Controllers\Admin\Siyosat_metodikaController::class);
    Route::resource('/siyosat_fanlar', \App\Http\Controllers\Admin\Siyosat_fanlarController::class);
    Route::resource('/savol-javob', \App\Http\Controllers\Admin\SavolController::class);
    Route::resource('/kutubxona', \App\Http\Controllers\Admin\KutubxonaController::class);
    Route::resource('/havola', \App\Http\Controllers\Admin\HavolaController::class);
    Route::resource('/fotogalareya', \App\Http\Controllers\Admin\FotogalareyaController::class);
    Route::resource('/elon', \App\Http\Controllers\Admin\ElonController::class);
    Route::resource('/yangilik', \App\Http\Controllers\Admin\YangilikController::class);

});


