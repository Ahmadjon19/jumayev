<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('sayt', [\App\Http\Controllers\Admin\SaytController::class, 'sayt_haqida1']);
Route::get('muallif', [\App\Http\Controllers\Admin\SaytController::class, 'muallif_haqida1']);
Route::get('farmon', [\App\Http\Controllers\Admin\FarmonController::class, 'view_farmon1']);
Route::get('hujjat', [\App\Http\Controllers\Admin\HujjatController::class, 'view_hujjat1']);
Route::get('tadqiqot', [\App\Http\Controllers\Admin\IlmiyController::class, 'view_tadqiqot1']);
Route::get('maqola', [\App\Http\Controllers\Admin\IlmiyController::class, 'view_maqola1']);
Route::get('lugat', [\App\Http\Controllers\Admin\LugatController::class, 'view_lugat1']);
Route::get('konferensiya', [\App\Http\Controllers\Admin\KonferensiyaController::class, 'view_konferensiya1']);
Route::get('akademik', [\App\Http\Controllers\Admin\AkademikController::class, 'view_akademik1']);
Route::get('katalog', [\App\Http\Controllers\Admin\KatalogController::class, 'view_katalog1']);
Route::get('kirish', [\App\Http\Controllers\Admin\Siyosat_kirishController::class, 'view_siyosat_kirish1']);
Route::get('metodika', [\App\Http\Controllers\Admin\Siyosat_metodikaController::class, 'view_siyosat_metodika1']);
Route::get('fanlar', [\App\Http\Controllers\Admin\Siyosat_fanlarController::class, 'view_siyosat_fanlar1']);
Route::get('savol', [\App\Http\Controllers\Admin\SavolController::class, 'view_savols1']);
Route::get('kutubxona', [\App\Http\Controllers\Admin\KutubxonaController::class, 'view_kutubxona1']);
Route::get('havola', [\App\Http\Controllers\Admin\HavolaController::class, 'view_havola1']);
Route::get('foto', [\App\Http\Controllers\Admin\FotogalareyaController::class, 'view_fotogalareya1']);
Route::get('elon', [\App\Http\Controllers\Admin\ElonController::class, 'view_elon1']);
Route::get('yangilik', [\App\Http\Controllers\Admin\YangilikController::class, 'view_yangilik1']);



