<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
//    public function index()
//    {
//        return view('home');
//    }

    public function adminpanel()
    {
        return view('admin.index');
    }

    public function edit()
    {
        return view('admin.setting');
    }

    public function update(User $user, Request $request)
    {
        $request->validate([
            'name'=>'required',
            'lname'=>'required',
            'email'=>'required|unique:users,email,'.$user->id,
            'old_password'=>['required', function($attribute, $value, $fail){
                if(!Hash::check($value, Auth::user()->password))
                {
                    return $fail('Old password is wrong');
                }
            }],
            'password'=>'required|min:8|confirmed',
        ]);

        $user->name = $request->input('name');
        $user->lname = $request->input('lname');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->update();
//        Alert::success('Success Title', 'Your info updated successfully');
        return redirect()->route('adminpanel');
    }
}
