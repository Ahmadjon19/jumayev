<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\siyosat_kirish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Siyosat_kirishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $siyosat_kirishs =siyosat_kirish::orderBy('id', 'desc')->get();
        return view('admin.siyosat_kirish.view', compact('siyosat_kirishs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.siyosat_kirish.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'file'=>'required',
        ]);
        $siyosat_kirish = new siyosat_kirish();
        $siyosat_kirish->name = $request->input('name');

        $siyosat_kirish_file = $request->file('file');
        $extension_file = $siyosat_kirish_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $siyosat_kirish->file = $orginal_name;
        $siyosat_kirish_file->move(public_path('admin/siyosat_kirish/'), $orginal_name);

        $siyosat_kirish->save();

        return redirect()->route('siyosat_kirish.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(siyosat_kirish $siyosat_kirish)
    {
        return view('admin.siyosat_kirish.edit', compact('siyosat_kirish'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, siyosat_kirish $siyosat_kirish)
    {
        $request->validate([
            'name'=>'required'
//            'file'=>'required',
        ]);

        if ($request->hasFile('file'))
        {
            File::delete(public_path('admin/siyosat_kirish/'.$siyosat_kirish->file));
            $siyosat_kirish_file = $request->file('file');
            $extension_file = $siyosat_kirish_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $siyosat_kirish->file = $orginal_name;
            $siyosat_kirish_file->move(public_path('admin/siyosat_kirish/'), $orginal_name);
        }
        $siyosat_kirish->name = $request->input('name');
        $siyosat_kirish->update();
        return redirect()->route('siyosat_kirish.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(siyosat_kirish $siyosat_kirish)
    {
        File::delete(public_path('admin/siyosat_kirish/'.$siyosat_kirish->file));
        siyosat_kirish::where('id', '=', $siyosat_kirish->id)->delete();
        $siyosat_kirish->delete();
        return redirect()->route('siyosat_kirish.index');
    }

    public function view_siyosat_kirish()
    {
        $siyosat_kirishs = siyosat_kirish::orderBy('id', 'desc')->get();
        return view('main.siyosat.kirish', compact('siyosat_kirishs'));
    }

    public function view_siyosat_kirish1()
    {
        $siyosat_kirishs = siyosat_kirish::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$siyosat_kirishs]);
    }
}
