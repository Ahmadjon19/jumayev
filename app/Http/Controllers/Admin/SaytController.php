<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\muallif;
use App\Models\sayt;
use Illuminate\Http\Request;

class SaytController extends Controller
{
    public function create_sayt()
    {
        return view('admin.sayt haqida.create');
    }

    public function iframe($file){
        return view('main.iframe',[
            'file' => $file
        ]);
    }

    public function store_sayt(Request $request)
    {
        $request->validate([
            'create_sayt'=>'required'
        ]);
        $sayt_store = new sayt();
        $create_sayt = $request->input('create_sayt');
        $sayt_store->about_sayt = $create_sayt;
        $sayt_store->save();
        return redirect()->route('view.sayt');
    }

    public function view_sayt()
    {
        $sayts = sayt::all();
        return view('admin.sayt haqida.view', compact('sayts'));
    }

    public function edit_sayt()
    {
        $sayts = sayt::all();
        return view('admin.sayt haqida.edit', compact('sayts'));

    }

    public function update_sayt(Request $request)
    {
        $request->validate([
            'edit_sayt'=>'required'
        ]);
        sayt::where('id', '>', 0)->delete();
        $sayt_store = new sayt();
        $create_sayt = $request->input('edit_sayt');
        $sayt_store->about_sayt = $create_sayt;
        $sayt_store->save();
        return redirect()->route('view.sayt');
    }

    public function create_muallif()
    {
        return view('admin.muallif haqida.create');
    }

    public function store_muallif(Request $request)
    {
        $request->validate([
            'create_muallif'=>'required'
        ]);
        $muallif_store = new muallif();
        $create_muallif = $request->input('create_muallif');
        $muallif_store->about_muallif = $create_muallif;
        $muallif_store->save();
        return redirect()->route('view.muallif');
    }

    public function view_muallif()
    {
        $muallifs = muallif::all();
        return view('admin.muallif haqida.view', compact('muallifs'));
    }

    public function edit_muallif()
    {
        $muallifs = muallif::all();
        return view('admin.muallif haqida.edit', compact('muallifs'));

    }

    public function update_muallif(Request $request)
    {
        $request->validate([
            'edit_muallif'=>'required'
        ]);
        muallif::where('id', '>', 0)->delete();
        $muallif_store = new muallif();
        $create_muallif = $request->input('edit_muallif');
        $muallif_store->about_muallif = $create_muallif;
        $muallif_store->save();
        return redirect()->route('view.muallif');
    }

    public function sayt_haqida()
    {
        $sayts = sayt::all();
        return view('main.sayt haqida.sayt haqida', compact('sayts'));

    }

    public function muallif_haqida()
    {
        $muallifs = muallif::all();
        return view('main.sayt haqida.muallif haqida', compact('muallifs'));

    }

    public function talim()
    {
        return view('main.talim');
    }

    public function ilova()
    {
        return view('main.ilova');
    }

    public function form()
    {
        return view('main.form');
    }

    public function sayt_haqida1()
    {
        $sayts = sayt::all();
        return response()->json(['success' => true, 'data' => $sayts]);
    }

    public function muallif_haqida1()
    {
        $muallifs = muallif::all();
        return response()->json(['success' => true, 'data' => $muallifs]);
    }
}
