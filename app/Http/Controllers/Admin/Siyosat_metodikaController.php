<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\siyosat_metodika;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Siyosat_metodikaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $siyosat_metodikas =siyosat_metodika::orderBy('id', 'desc')->get();
        return view('admin.siyosat_metodika.view', compact('siyosat_metodikas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.siyosat_metodika.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'file'=>'required',
        ]);
        $siyosat_metodika = new siyosat_metodika();
        $siyosat_metodika->name = $request->input('name');

        $siyosat_metodika_file = $request->file('file');
        $extension_file = $siyosat_metodika_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $siyosat_metodika->file = $orginal_name;
        $siyosat_metodika_file->move(public_path('admin/siyosat_metodika/'), $orginal_name);

        $siyosat_metodika->save();

        return redirect()->route('siyosat_metodika.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(siyosat_metodika $siyosat_metodika)
    {
        return view('admin.siyosat_metodika.edit', compact('siyosat_metodika'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, siyosat_metodika $siyosat_metodika)
    {
        $request->validate([
            'name'=>'required'
//            'file'=>'required',
        ]);

        if ($request->hasFile('file'))
        {
            File::delete(public_path('admin/siyosat_metodika/'.$siyosat_metodika->file));
            $siyosat_metodika_file = $request->file('file');
            $extension_file = $siyosat_metodika_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $siyosat_metodika->file = $orginal_name;
            $siyosat_metodika_file->move(public_path('admin/siyosat_metodika/'), $orginal_name);
        }
        $siyosat_metodika->name = $request->input('name');
        $siyosat_metodika->update();
        return redirect()->route('siyosat_metodika.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(siyosat_metodika $siyosat_metodika)
    {
        File::delete(public_path('admin/siyosat_metodika/'.$siyosat_metodika->file));
        siyosat_metodika::where('id', '=', $siyosat_metodika->id)->delete();
        $siyosat_metodika->delete();
        return redirect()->route('siyosat_metodika.index');
    }

    public function view_siyosat_metodika()
    {
        $siyosat_metodikas = siyosat_metodika::orderBy('id', 'desc')->get();
        return view('main.siyosat.metodika', compact('siyosat_metodikas'));
    }

    public function view_siyosat_metodika1()
    {
        $siyosat_metodikas = siyosat_metodika::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$siyosat_metodikas]);
    }
}
