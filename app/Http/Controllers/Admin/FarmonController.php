<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\farmon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FarmonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $farmons = farmon::orderBy('id', 'desc')->get();
        return view('admin.farmon.view', compact('farmons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.farmon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'farmon_name'=>'required',
//            'farmon_file'=>'required',
            'farmon_type'=>'required'
        ]);
        $farmon = new farmon();
        $farmon->name = $request->input('farmon_name');

//        $farmon_file = $request->file('farmon_file');
//        $extension_file = $farmon_file->getClientOriginalExtension();
//        $orginal_name = time().'.'.$extension_file;
//        $farmon->fayl = $orginal_name;
//        $farmon_file->move(public_path('admin/'.$request->input('farmon_type').'/'), $orginal_name);
        $farmon->fayl = "";
        $farmon->turi = $request->input('farmon_type');
        $farmon->save();

        return redirect()->route('farmon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(farmon $farmon)
    {
        return view('admin.farmon.edit', compact('farmon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, farmon $farmon)
    {
        $request->validate([
            'farmon_name'=>'required',
//            'farmon_file'=>'required',
            'farmon_type'=>'required'
        ]);

//        if ($request->hasFile('farmon_file'))
//        {
//            File::delete(public_path('admin/'.$farmon->turi.'/'.$farmon->fayl));
//            $farmon_file = $request->file('farmon_file');
//            $extension_file = $farmon_file->getClientOriginalExtension();
//            $orginal_name = time().'.'.$extension_file;
//            $farmon->fayl = $orginal_name;
//            $farmon_file->move(public_path('admin/'.$request->input('farmon_type').'/'), $orginal_name);
//        }
        $farmon->fayl = "";
        $farmon->name = $request->input('farmon_name');
        $farmon->turi = $request->input('farmon_type');
        $farmon->update();
        return redirect()->route('farmon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(farmon $farmon)
    {
//        File::delete(public_path('admin/'.$farmon->turi.'/'.$farmon->fayl));
        farmon::where('id', '=', $farmon->id)->delete();
        $farmon->delete();
        return redirect()->route('farmon.index');
    }

    public function view_farmon()
    {
        $farmons = farmon::orderBy('id', 'desc')->get();
        return view('main.normativ hujjatlar.farmon', compact('farmons'));
    }

    public function view_farmon1()
    {
        $farmons = farmon::orderBy('id', 'desc')->get();
        return response()->json(['success' => true, 'data' => $farmons]);
    }
}
