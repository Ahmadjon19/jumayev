<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\yangilik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class YangilikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $fotos = yangilik::orderBy('id', 'desc')->get();
        return view('admin.yangilik.view', compact('fotos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.yangilik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'desc'=>'required',
            'file'=>'required|array',
            'file.*'=>'mimes:jpeg,png,jpg,svg'
        ]);
        $org_images = "";
        for ($i=0; $i<count($request->file('file')); $i++)
        {
            $all_img = $request->file('file')[$i];
            $extension = $all_img->getClientOriginalExtension();
            $fileNameToStore = time();
            $org_images .= $fileNameToStore.$i.'.'.$extension.',';
            $all_img->move(public_path('admin/yangilik'), $fileNameToStore.$i.'.'.$extension);
        }

        $foto = new yangilik();
        $foto->name = $request->input('name');
        $foto->matn = $request->input('desc');
        $foto->rasm = $org_images;
        $foto->save();

        return redirect()->route('yangilik.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($foto)
    {
        $fotos = yangilik::where('id', '=', $foto)->first();
        $images = explode(',', $fotos->rasm);
        return view('admin.yangilik.show', compact('fotos', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($foto)
    {
        $fotos = yangilik::where('id', '=', $foto)->first();
        return view('admin.yangilik.edit', compact('fotos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $foto)
    {
        $request->validate([
            'name'=>'required',
            'desc'=>'required',
            'file.*'=>'mimes:jpeg,png,jpg,svg'
        ]);
        $fotos = yangilik::where('id', '=', $foto)->first();
        if($request->hasFile('file'))
        {
            $old_images_org = explode(',', $fotos->image);
            for ($i=0; $i<count($old_images_org)-1; $i++)
            {
                File::delete(public_path('admin/yangilik/'.$old_images_org[$i]));
            }

            $org_images = '';
            for ($i=0; $i<count($request->file('file')); $i++)
            {
                $all_img = $request->file('file')[$i];
                $extension = $all_img->getClientOriginalExtension();
                $fileNameToStore = time();
                $org_images .= $fileNameToStore.$i.'.'.$extension.',';
                $all_img->move(public_path('admin/yangilik'), $fileNameToStore.$i.'.'.$extension);
            }
        }else{
            $org_images = $fotos->rasm;
        }

        $fotos->name = $request->input('name');
        $fotos->matn = $request->input('desc');
        $fotos->rasm = $org_images;
        $fotos->update();

        return redirect()->route('yangilik.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($foto)
    {
        $data = yangilik::where('id', '=', $foto)->first();
        $images = explode(",", $data->rasm);
        for($i=0; $i<count($images)-1; $i++){
            File::delete(public_path('admin/yangilik/'.$images[$i]));
        }
        yangilik::where('id', '=', $foto)->delete();
        return redirect()->route('yangilik.index');
    }

    public function view_yangilik()
    {
        $fotos = yangilik::all();
        $all_image = [];
        for ($i=0; $i<count($fotos); $i++){
            $images = explode(',', $fotos[$i]->rasm);
            $all_image[$i] = $images;
        }

        return view('main.yangiliklar.yangilik', compact('fotos', 'all_image'));
    }

    public function view_yangilik1()
    {
        $fotos = yangilik::all();
        $all_image = [];
        for ($i=0; $i<count($fotos); $i++){
            $images = explode(',', $fotos[$i]->rasm);
            $all_image[$i] = $images;
        }
        return response()->json(['success'=>true, 'data'=>$fotos, 'data1' => $all_image]);
    }
}
