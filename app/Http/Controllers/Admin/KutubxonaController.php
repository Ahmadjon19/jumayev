<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\kutubxona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class KutubxonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $kutubxonas = kutubxona::orderBy('id', 'desc')->get();
        return view('admin.kutubxona.view', compact('kutubxonas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kutubxona.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'file'=>'required',
        ]);
        $kutubxona = new kutubxona();
        $kutubxona->name = $request->input('name');

        $kutubxona_file = $request->file('file');
        $extension_file = $kutubxona_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $kutubxona->file = $orginal_name;
        $kutubxona_file->move(public_path('admin/kutubxona/'), $orginal_name);

        $kutubxona->save();

        return redirect()->route('kutubxona.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(kutubxona $kutubxona)
    {
        return view('admin.kutubxona.edit', compact('kutubxona'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, kutubxona $kutubxona)
    {
        $request->validate([
            'name'=>'required'
//            'file'=>'required',
        ]);

        if ($request->hasFile('file'))
        {
            File::delete(public_path('admin/kutubxona/'.$kutubxona->file));
            $kutubxona_file = $request->file('file');
            $extension_file = $kutubxona_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $kutubxona->file = $orginal_name;
            $kutubxona_file->move(public_path('admin/kutubxona/'), $orginal_name);
        }
        $kutubxona->name = $request->input('name');
        $kutubxona->update();
        return redirect()->route('kutubxona.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(kutubxona $kutubxona)
    {
        File::delete(public_path('admin/kutubxona/'.$kutubxona->file));
        kutubxona::where('id', '=', $kutubxona->id)->delete();
        $kutubxona->delete();
        return redirect()->route('kutubxona.index');
    }

    public function view_kutubxona()
    {
        $kutubxonas = kutubxona::orderBy('id', 'desc')->get();
        return view('main.kutubxona.kutubxona', compact('kutubxonas'));
    }

    public function view_kutubxona1()
    {
        $kutubxonas = kutubxona::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$kutubxonas]);
    }
}
