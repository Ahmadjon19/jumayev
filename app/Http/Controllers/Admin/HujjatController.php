<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\hujjat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class HujjatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $hujjats = hujjat::orderBy('id', 'desc')->get();
        return view('admin.hujjat.view', compact('hujjats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hujjat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'hujjat_name'=>'required',
            'hujjat_file'=>'required',
            'hujjat_type'=>'required'
        ]);
        $hujjat = new hujjat();
        $hujjat->hujjat_nomi = $request->input('hujjat_name');

        $hujjat_file = $request->file('hujjat_file');
        $extension_file = $hujjat_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $hujjat->hujjat_fayl = $orginal_name;
        $hujjat_file->move(public_path('admin/'.$request->input('hujjat_type').'/'), $orginal_name);

        $hujjat->hujjat_turi = $request->input('hujjat_type');
        $hujjat->save();

        return redirect()->route('hujjat.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(hujjat $hujjat)
    {
        return view('admin.hujjat.edit', compact('hujjat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, hujjat $hujjat)
    {
        $request->validate([
            'hujjat_name'=>'required',
//            'hujjat_file'=>'required',
            'hujjat_type'=>'required'
        ]);

        if ($request->hasFile('hujjat_file'))
        {
            File::delete(public_path('admin/'.$hujjat->hujjat_turi.'/'.$hujjat->hujjat_fayl));
            $hujjat_file = $request->file('hujjat_file');
            $extension_file = $hujjat_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $hujjat->hujjat_fayl = $orginal_name;
            $hujjat_file->move(public_path('admin/'.$request->input('hujjat_type').'/'), $orginal_name);
        }
//        if ($request->input('hujjat_type') != $hujjat->hujjat_turi)
//        {
//            $hujjat_file = $hujjat->hujjat_fayl;
//            $hujjat_file->move(public_path('admin/'.$request->input('hujjat_type').'/'), $hujjat_file);
//        }
        $hujjat->hujjat_nomi = $request->input('hujjat_name');
        $hujjat->hujjat_turi = $request->input('hujjat_type');
        $hujjat->update();
        return redirect()->route('hujjat.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(hujjat $hujjat)
    {
        File::delete(public_path('admin/'.$hujjat->hujjat_turi.'/'.$hujjat->hujjat_fayl));
        hujjat::where('id', '=', $hujjat->id)->delete();
        $hujjat->delete();
        return redirect()->route('hujjat.index');
    }

    public function view_hujjat()
    {
        $hujjats = hujjat::orderBy('id', 'desc')->get();
        return view('main.normativ hujjatlar.hujjat', compact('hujjats'));
    }

    public function view_hujjat1()
    {
        $hujjats = hujjat::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$hujjats]);
    }
}
