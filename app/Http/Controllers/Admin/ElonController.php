<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\elon;
use Illuminate\Http\Request;

class ElonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $elons = elon::orderBy('id', 'desc')->get();
        return view('admin.elonlar.view', compact('elons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elonlar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'elon'=>'required'
        ]);
        $elon = new elon();
        $elons = $request->input('elon');
        $elon->elon = $elons;
        $elon->save();
        return redirect()->route('elon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($elon)
    {
        $elons = elon::where('id', '=', $elon)->first();
        return view('admin.elonlar.show', compact('elons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($elon)
    {
        $elons = elon::where('id', '=', $elon)->first();
        return view('admin.elonlar.edit', compact('elons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $elons)
    {
        $request->validate([
            'edit'=>'required'
        ]);
        $elon = elon::where('id', '=', $elons)->first();
        $elons1 = $request->input('edit');
        $elon->elon = $elons1;
        $elon->update();
        return redirect()->route('elon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($elon)
    {
        elon::where('id', '=', $elon)->delete();
        return redirect()->route('elon.index');
    }

    public function view_elon()
    {
        $elons = elon::all();
        return view('main.yangiliklar.elon', compact('elons'));
    }

    public function view_elon1()
    {
        $elons = elon::all();
        return response()->json(['success'=>true, 'data'=>$elons]);
    }
}
