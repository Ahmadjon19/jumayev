<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\havola;
use Illuminate\Http\Request;

class HavolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $havolas = havola::orderBy('id', 'desc')->get();
        return view('admin.havola.view', compact('havolas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.havola.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'havola'=>'required',
        ]);
        $havola = new havola();
        $havola->name = $request->input('name');
        $havola->havola = $request->input('havola');
        $havola->save();

        return redirect()->route('havola.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(havola $havola)
    {
        return view('admin.havola.edit', compact('havola'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, havola $havola)
    {
        $request->validate([
            'name'=>'required',
            'havola'=>'required',
        ]);
        $havola->name = $request->input('name');
        $havola->havola = $request->input('havola');
        $havola->update();

        return redirect()->route('havola.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(havola $havola)
    {
        havola::where('id', '=', $havola->id)->delete();
        $havola->delete();
        return redirect()->route('havola.index');
    }

    public function view_havola()
    {
        $havolas = havola::orderBy('id', 'desc')->get();
        return view('main.kutubxona.havola', compact('havolas'));
    }

    public function view_havola1()
    {
        $havolas = havola::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$havolas]);
    }
}
