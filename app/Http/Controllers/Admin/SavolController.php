<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\savol;
use Illuminate\Http\Request;

class SavolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $savols =savol::orderBy('id', 'desc')->get();
        return view('admin.savol-javob.view', compact('savols'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.savol-javob.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'savol'=>'required',
            'javob'=>'required',
        ]);
        $savol = new savol();
        $savol->savol = $request->input('savol');
        $savol->javob = $request->input('javob');

        $savol->save();

        return redirect()->route('savol-javob.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($savol)
    {
        $savols = savol::where('id', '=', $savol)->first();
        return view('admin.savol-javob.edit', compact('savols'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $savols)
    {
        $request->validate([
            'savol'=>'required',
            'javob'=>'required'
        ]);
        $savol = savol::where('id', '=', $savols)->first();
        $savol->savol = $request->input('savol');
        $savol->javob = $request->input('javob');
        $savol->update();
        return redirect()->route('savol-javob.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($savol)
    {
        savol::where('id', '=', $savol)->delete();
//        $savol->delete();
        return redirect()->route('savol-javob.index');
    }

    public function view_savols()
    {
        $savols = savol::orderBy('id', 'desc')->get();
        return view('main.kutubxona.savol', compact('savols'));
    }

    public function view_savols1()
    {
        $savols = savol::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$savols]);
    }
}
