<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\fotogalareya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FotogalareyaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $fotos = fotogalareya::orderBy('id', 'desc')->get();
        return view('admin.fotogalareya.view', compact('fotos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.fotogalareya.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'desc'=>'required',
            'file'=>'required|array',
            'file.*'=>'mimes:jpeg,png,jpg,svg'
        ]);
        $org_images = "";
        for ($i=0; $i<count($request->file('file')); $i++)
        {
            $all_img = $request->file('file')[$i];
            $extension = $all_img->getClientOriginalExtension();
            $fileNameToStore = time();
            $org_images .= $fileNameToStore.$i.'.'.$extension.',';
            $all_img->move(public_path('admin/fotogalareya'), $fileNameToStore.$i.'.'.$extension);
        }

        $foto = new fotogalareya();
        $foto->name = $request->input('name');
        $foto->matn = $request->input('desc');
        $foto->image = $org_images;
        $foto->save();

        return redirect()->route('fotogalareya.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($foto)
    {
        $fotos = fotogalareya::where('id', '=', $foto)->first();
        $images = explode(',', $fotos->image);
        return view('admin.fotogalareya.show', compact('fotos', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($foto)
    {
        $fotos = fotogalareya::where('id', '=', $foto)->first();
        return view('admin.fotogalareya.edit', compact('fotos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $foto)
    {
        $request->validate([
            'name'=>'required',
            'desc'=>'required',
            'file.*'=>'mimes:jpeg,png,jpg,svg'
        ]);
        $fotos = fotogalareya::where('id', '=', $foto)->first();
        if($request->hasFile('file'))
        {
            $old_images_org = explode(',', $fotos->image);
            for ($i=0; $i<count($old_images_org)-1; $i++)
            {
                File::delete(public_path('admin/fotogalareya/'.$old_images_org[$i]));
            }

            $org_images = '';
            for ($i=0; $i<count($request->file('file')); $i++)
            {
                $all_img = $request->file('file')[$i];
                $extension = $all_img->getClientOriginalExtension();
                $fileNameToStore = time();
                $org_images .= $fileNameToStore.$i.'.'.$extension.',';
                $all_img->move(public_path('admin/fotogalareya'), $fileNameToStore.$i.'.'.$extension);
            }
        }else{
            $org_images = $fotos->image;
        }

        $fotos->name = $request->input('name');
        $fotos->matn = $request->input('desc');
        $fotos->image = $org_images;
        $fotos->update();

        return redirect()->route('fotogalareya.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($foto)
    {
        $data = fotogalareya::where('id', '=', $foto)->first();
        $images = explode(",", $data->image);
        for($i=0; $i<count($images)-1; $i++){
            File::delete(public_path('admin/fotogalareya/'.$images[$i]));
        }
        fotogalareya::where('id', '=', $foto)->delete();
        return redirect()->route('fotogalareya.index');
    }

    public function view_fotogalareya()
    {
        $fotos = fotogalareya::all();
        $all_image = [];
        for ($i=0; $i<count($fotos); $i++){
            $images = explode(',', $fotos[$i]->image);
            $all_image[$i] = $images;
        }
        return view('main.kutubxona.fotogalareya', compact('fotos', 'all_image'));
    }

    public function view_fotogalareya1()
    {
        $fotos = fotogalareya::all();
        $all_image = [];
        for ($i=0; $i<count($fotos); $i++){
            $images = explode(',', $fotos[$i]->image);
            $all_image[$i] = $images;
        }
        return response()->json(['success'=>true, 'data'=>$fotos, 'data1' => $all_image]);
    }
}
