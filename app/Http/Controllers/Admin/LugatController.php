<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\lugat;
use Illuminate\Http\Request;

class LugatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $lugats = lugat::orderBy('id', 'desc')->get();
        return view('admin.lugat.view', compact('lugats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lugat.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'suz'=>'required',
            'mano'=>'required'
        ]);
        $lugat = new lugat();
        $lugat->suz = $request->input('suz');
        $lugat->mano = $request->input('mano');
        $lugat->save();
        return redirect()->route('lugat.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(lugat $lugat)
    {
        return view('admin.lugat.edit', compact('lugat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, lugat $lugat)
    {
        $request->validate([
            'suz'=>'required',
            'mano'=>'required'
        ]);

        $lugat->suz = $request->input('suz');
        $lugat->mano = $request->input('mano');
        $lugat->update();
        return redirect()->route('lugat.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(lugat $lugat)
    {
        lugat::where('id', '=', $lugat->id)->delete();
        $lugat->delete();
        return redirect()->route('lugat.index');
    }

    public function view_lugat()
    {
        $lugats = lugat::orderBy('id', 'desc')->get();
        return view('main.kutubxona.elektron_lugat', compact('lugats'));
    }

    public function view_lugat1()
    {
        $lugats = lugat::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$lugats]);
    }
}
