<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\katalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class KatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $katalogs = katalog::orderBy('id', 'desc')->get();
        return view('admin.katalog.view', compact('katalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.katalog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'file'=>'required',
        ]);
        $katalog = new katalog();
        $katalog->name = $request->input('name');

        $katalog_file = $request->file('file');
        $extension_file = $katalog_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $katalog->file = $orginal_name;
        $katalog_file->move(public_path('admin/dastur katalogi/'), $orginal_name);

        $katalog->save();

        return redirect()->route('katalog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(katalog $katalog)
    {
        return view('admin.katalog.edit', compact('katalog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, katalog $katalog)
    {
        $request->validate([
            'name'=>'required'
//            'file'=>'required',
        ]);

        if ($request->hasFile('file'))
        {
            File::delete(public_path('admin/dastur katalogi/'.$katalog->file));
            $katalog_file = $request->file('file');
            $extension_file = $katalog_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $katalog->file = $orginal_name;
            $katalog_file->move(public_path('admin/dastur katalogi/'), $orginal_name);
        }
        $katalog->name = $request->input('name');
        $katalog->update();
        return redirect()->route('katalog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(katalog $katalog)
    {
        File::delete(public_path('admin/dastur katalogi/'.$katalog->file));
        katalog::where('id', '=', $katalog->id)->delete();
        $katalog->delete();
        return redirect()->route('katalog.index');
    }

    public function view_katalog()
    {
        $katalogs = katalog::orderBy('id', 'desc')->get();
        return view('main.dastur.katalog', compact('katalogs'));
    }

    public function view_katalog1()
    {
        $katalogs = katalog::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$katalogs]);
    }
}
