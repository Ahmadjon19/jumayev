<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\akademik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AkademikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $akademiks = akademik::orderBy('id', 'desc')->get();
        return view('admin.dastur.view', compact('akademiks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dastur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'file'=>'required',
        ]);
        $akademik = new akademik();
        $akademik->name = $request->input('name');

        $akademik_file = $request->file('file');
        $extension_file = $akademik_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $akademik->file = $orginal_name;
        $akademik_file->move(public_path('admin/akademik dasturlar/'), $orginal_name);

        $akademik->save();

        return redirect()->route('akademik.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(akademik $akademik)
    {
        return view('admin.dastur.edit', compact('akademik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, akademik $akademik)
    {
        $request->validate([
            'name'=>'required'
//            'file'=>'required',
        ]);

        if ($request->hasFile('file'))
        {
            File::delete(public_path('admin/akademik dasturlar/'.$akademik->file));
            $akademik_file = $request->file('file');
            $extension_file = $akademik_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $akademik->file = $orginal_name;
            $akademik_file->move(public_path('admin/akademik dasturlar/'), $orginal_name);
        }
        $akademik->name = $request->input('name');
        $akademik->update();
        return redirect()->route('akademik.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(akademik $akademik)
    {
        File::delete(public_path('admin/akademik dasturlar/'.$akademik->file));
        akademik::where('id', '=', $akademik->id)->delete();
        $akademik->delete();
        return redirect()->route('akademik.index');
    }

    public function view_akademik()
    {
        $akademiks = akademik::orderBy('id', 'desc')->get();
        return view('main.dastur.akademik', compact('akademiks'));
    }

    public function view_akademik1()
    {
        $akademiks = akademik::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$akademiks]);
    }
}
