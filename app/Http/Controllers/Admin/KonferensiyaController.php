<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\konferensiya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class KonferensiyaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $konferensiyas = konferensiya::orderBy('id', 'desc')->get();
        return view('admin.konferensiya.view', compact('konferensiyas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.konferensiya.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'mavzusi'=>'required',
            'nizom'=>'required',
            'muddat'=>'required'
        ]);
        $konferensiya = new konferensiya();
        $konferensiya->mavzusi = $request->input('mavzusi');

        $konferensiya_file = $request->file('nizom');
        $extension_file = $konferensiya_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $konferensiya->nizom = $orginal_name;
        $konferensiya_file->move(public_path('admin/konferensiya/'), $orginal_name);

        $konferensiya->muddat = $request->input('muddat');
        $konferensiya->save();

        return redirect()->route('konferensiya.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(konferensiya $konferensiya)
    {
        return view('admin.konferensiya.edit', compact('konferensiya'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, konferensiya $konferensiya)
    {
        $request->validate([
            'mavzusi'=>'required',
//            'farmon_file'=>'required',
            'muddat'=>'required'
        ]);

        if ($request->hasFile('nizom'))
        {
            File::delete(public_path('admin/konferensiya/'.$konferensiya->nizom));
            $konferensiya_file = $request->file('nizom');
            $extension_file = $konferensiya_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $konferensiya->nizom = $orginal_name;
            $konferensiya_file->move(public_path('admin/konferensiya/'), $orginal_name);
        }
        $konferensiya->mavzusi = $request->input('mavzusi');
        $konferensiya->muddat = $request->input('muddat');
        $konferensiya->update();
        return redirect()->route('konferensiya.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(konferensiya $konferensiya)
    {
        File::delete(public_path('admin/konferensiya/'.$konferensiya->nizom));
        konferensiya::where('id', '=', $konferensiya->id)->delete();
        $konferensiya->delete();
        return redirect()->route('konferensiya.index');
    }

    public function view_konferensiya()
    {
        $konferensiyas = konferensiya::orderBy('id', 'desc')->get();
        return view('main.ilmiy.konferensiya', compact('konferensiyas'));
    }

    public function view_konferensiya1()
    {
        $konferensiyas = konferensiya::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$konferensiyas]);
    }
}
