<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ilmiy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class IlmiyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $ilmiys = ilmiy::orderBy('id', 'desc')->get();
        return view('admin.ilmiy.view', compact('ilmiys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ilmiy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'ilmiy_name'=>'required',
            'ilmiy_file'=>'required',
            'ilmiy_type'=>'required',
            'ilmiy_desc'=>'required'
        ]);
        $ilmiy = new ilmiy();
        $ilmiy->name = $request->input('ilmiy_name');

        $ilmiy_file = $request->file('ilmiy_file');
        $extension_file = $ilmiy_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        $ilmiy->fayl = $orginal_name;
        $ilmiy_file->move(public_path('admin/'.$request->input('ilmiy_type').'/'), $orginal_name);

        $ilmiy->turi = $request->input('ilmiy_type');
        $ilmiy->izoh = $request->input('ilmiy_desc');
        $ilmiy->save();

        return redirect()->route('ilmiy.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(ilmiy $ilmiy)
    {
        return view('admin.ilmiy.edit', compact('ilmiy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ilmiy $ilmiy)
    {
        $request->validate([
            'ilmiy_name'=>'required',
//            'ilmiy_file'=>'required',
            'ilmiy_type'=>'required',
            'ilmiy_desc'=>'required'
        ]);

        if ($request->hasFile('ilmiy_file'))
        {
            File::delete(public_path('admin/'.$ilmiy->turi.'/'.$ilmiy->fayl));
            $ilmiy_file = $request->file('ilmiy_file');
            $extension_file = $ilmiy_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            $ilmiy->fayl = $orginal_name;
            $ilmiy_file->move(public_path('admin/'.$request->input('ilmiy_type').'/'), $orginal_name);
        }
        $ilmiy->name = $request->input('ilmiy_name');
        $ilmiy->turi = $request->input('ilmiy_type');
        $ilmiy->izoh = $request->input('ilmiy_desc');
        $ilmiy->update();
        return redirect()->route('ilmiy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(ilmiy $ilmiy)
    {
        File::delete(public_path('admin/'.$ilmiy->turi.'/'.$ilmiy->fayl));
        ilmiy::where('id', '=', $ilmiy->id)->delete();
        $ilmiy->delete();
        return redirect()->route('ilmiy.index');
    }

    public function view_tadqiqot()
    {
        $tadqiqots = ilmiy::where('turi', '=', 'tadqiqot')->orderBy('id', 'desc')->get();
        return view('main.ilmiy.tadqiqot', compact('tadqiqots'));
    }

    public function view_maqola()
    {
        $maqolas = ilmiy::where('turi', '=', 'maqola')->orderBy('id', 'desc')->get();
        return view('main.ilmiy.maqola', compact('maqolas'));
    }

    public function view_tadqiqot1()
    {
        $tadqiqots = ilmiy::where('turi', '=', 'tadqiqot')->orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$tadqiqots]);
    }

    public function view_maqola1()
    {
        $maqolas = ilmiy::where('turi', '=', 'maqola')->orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$maqolas]);
    }
}
