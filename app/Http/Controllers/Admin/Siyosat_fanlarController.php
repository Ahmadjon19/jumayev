<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\siyosat_fanlar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Siyosat_fanlarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $siyosat_fanlars = siyosat_fanlar::orderBy('id', 'desc')->get();
        return view('admin.siyosat_fanlar.view', compact('siyosat_fanlars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.siyosat_fanlar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'file' => 'required',
        ]);
        $siyosat_fanlar = new siyosat_fanlar();
        $siyosat_fanlar->name = $request->input('name');

        $siyosat_fanlar_file = $request->file('file');
        $extension_file = $siyosat_fanlar_file->getClientOriginalExtension();
        $orginal_name = time() . '.' . $extension_file;
        $siyosat_fanlar->file = $orginal_name;
        $siyosat_fanlar_file->move(public_path('admin/siyosat_fanlar/'), $orginal_name);

        $siyosat_fanlar->save();

        return redirect()->route('siyosat_fanlar.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(siyosat_fanlar $siyosat_fanlar)
    {
        return view('admin.siyosat_fanlar.edit', compact('siyosat_fanlar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, siyosat_fanlar $siyosat_fanlar)
    {
        $request->validate([
            'name' => 'required'
//            'file'=>'required',
        ]);

        if ($request->hasFile('file')) {
            File::delete(public_path('admin/siyosat_fanlar/' . $siyosat_fanlar->file));
            $siyosat_fanlar_file = $request->file('file');
            $extension_file = $siyosat_fanlar_file->getClientOriginalExtension();
            $orginal_name = time() . '.' . $extension_file;
            $siyosat_fanlar->file = $orginal_name;
            $siyosat_fanlar_file->move(public_path('admin/siyosat_fanlar/'), $orginal_name);
        }
        $siyosat_fanlar->name = $request->input('name');
        $siyosat_fanlar->update();
        return redirect()->route('siyosat_fanlar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(siyosat_fanlar $siyosat_fanlar)
    {
        File::delete(public_path('admin/siyosat_fanlar/' . $siyosat_fanlar->file));
        siyosat_fanlar::where('id', '=', $siyosat_fanlar->id)->delete();
        $siyosat_fanlar->delete();
        return redirect()->route('siyosat_fanlar.index');
    }

    public function view_siyosat_fanlar()
    {
        $siyosat_fanlars = siyosat_fanlar::orderBy('id', 'desc')->get();
        return view('main.siyosat.fanlar', compact('siyosat_fanlars'));
    }

    public function view_siyosat_fanlar1()
    {
        $siyosat_fanlars = siyosat_fanlar::orderBy('id', 'desc')->get();
        return response()->json(['success'=>true, 'data'=>$siyosat_fanlars]);
    }
}
