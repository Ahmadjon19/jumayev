@extends('admin.layouts.master')
@section('title', ' Barcha fotogalareyalar')
@section('content')

    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha fotogalareyalar</h5>
                <a href="{{route('fotogalareya.create')}}" class="btn btn-primary">Fotogalareya qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Mavzu</th>
                            <th>Izoh</th>
{{--                            <th>Qachon qo'shilgan</th>--}}
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fotos as $key=>$foto)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$foto->name}}</td>
                                <td>{{$foto->matn}}</td>
{{--                                <td>{{$foto->created_at}}</td>--}}
                                <td class="d-flex" >
                                    <a style="height: 34px; width:100px; padding: 10px; line-height: normal; margin-top: 21px; margin-right: 5px"  href="{{route('fotogalareya.edit', $foto)}}" class="btn btn-success">Tahrirlash</a>
                                    <a style="height: 34px; width:75px; padding: 10px; line-height: normal; margin-top: 21px; margin-right: 5px"  href="{{route('fotogalareya.show', $foto)}}" class="btn btn-success">Ko'rish</a>
                                    <form action="{{route('fotogalareya.destroy', $foto)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete fotogalareya?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
