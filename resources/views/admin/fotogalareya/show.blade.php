@extends('admin.layouts.master')
@section('title', 'Fotogalareyani ko\'rish')
@section('content')
    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$fotos->name}}</h5>
                    </div>
                    <div class="card-body">
                        <p>{{$fotos->matn}}</p>
                    </div>
                    <div class="card-body">
                        <div class="row my-gallery gallery" id="aniimated-thumbnials" itemscope="" data-pswp-uid="1">
                            @for($i=0; $i<count($images)-1; $i++)
                            <figure class="col-md-3 col-6 img-hover hover-1" itemprop="associatedMedia" itemscope="">
                                <a href="{{asset('admin/fotogalareya/'.$images[$i])}}" itemprop="contentUrl" data-size="1600x950" data-bs-original-title="" title="">
                                    <div><img src="{{asset('admin/fotogalareya/'.$images[$i])}}" itemprop="thumbnail" alt="Image description"></div>
                                </a>
                                <figcaption itemprop="caption description"></figcaption>
                            </figure>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
