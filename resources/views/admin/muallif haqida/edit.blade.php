@extends('admin.layouts.master')
@section('title', 'Muallif tahrirlash')
@section('content')
    <form action="{{route('update.muallif')}}" method="post">
        @csrf
        <br>
        <div>
            <textarea class="@error('edit_muallif') is-invalid @enderror" id="editor1" name="edit_muallif" style="min-height: 1500px !important;">
                @foreach($muallifs as $muallif)
                    {!! $muallif->about_muallif !!}
                @endforeach
            </textarea>
            @error('edit_muallif')
            <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
            @enderror
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Yangilash</button>
    </form>
@endsection
