@extends('admin.layouts.master')
@section('title', 'Muallif yaratish')
@section('content')
    <br>
    <form action="{{route('store.muallif')}}" method="post">
        @csrf
        <div>
            <textarea class="@error('create_muallif') is-invalid @enderror" id="editor1" name="create_muallif" cols="30" rows="10">

            </textarea>
            @error('create_muallif')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
            @enderror
        </div>
        <br>
        <button class="btn btn-success" type="submit">Saqlash</button>
    </form>
@endsection
