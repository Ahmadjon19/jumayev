@extends('admin.layouts.master')
@section('title', 'Hujjat qo\'shish')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('hujjat.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Hujjat qo'shish</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Hujjat nomi</div>
                                <input type="text" name="hujjat_name" class="form-control @error('hujjat_name') is-invalid @enderror">
                                @error('hujjat_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Hujjat fayli</div>
                                <input type="file" name="hujjat_file" class="form-control @error('hujjat_file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                @error('hujjat_file')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Hujjat turi</div>
                                <input type="text" name="hujjat_type" class="form-control @error('hujjat_type') is-invalid @enderror">
                                @error('hujjat_type')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Saqlash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
