@extends('admin.layouts.master')
@section('title', 'Hujjatlarni ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha hujjatlar</h5>
                <a href="{{route('hujjat.create')}}" class="btn btn-primary">Hujjat qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Hujjat nomi</th>
{{--                            <th>Hujjat file</th>--}}
                            <th>Hujjat turi</th>
                            <th>Qachon yaratilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($hujjats as $key=>$hujjat)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$hujjat->hujjat_nomi}}</td>--}}
                                <td><a href="{{asset('admin/'.$hujjat->hujjat_turi.'/'.$hujjat->hujjat_fayl)}}">{{$hujjat->hujjat_nomi}}</a></td>
                                <td>{{$hujjat->hujjat_turi}}</td>
                                <td>{{$hujjat->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('hujjat.edit', $hujjat)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('hujjat.destroy', $hujjat)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete hujjat?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
