@extends('admin.layouts.master')
@section('title', 'Kitoblarni ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha kitoblarni ko'rish</h5>
                <a href="{{route('siyosat_kirish.create')}}" class="btn btn-primary">Kitob qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Kitob nomi</th>
{{--                            <th>Kitob file</th>--}}
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($siyosat_kirishs as $key=>$siyosat_kirish)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$siyosat_kirish->name}}</td>--}}
                                <td><a href="{{asset('admin/siyosat_kirish/'.$siyosat_kirish->file)}}">{{$siyosat_kirish->name}}</a></td>
                                <td>{{$siyosat_kirish->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('siyosat_kirish.edit', $siyosat_kirish)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('siyosat_kirish.destroy', $siyosat_kirish)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete siyosatshunoslik?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
