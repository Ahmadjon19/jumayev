@extends('admin.layouts.master')
@section('title', 'Lug\'atni tahrirlash')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('lugat.update', $lugat)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Lug'atni tahrirlash</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">So'z</div>
                                <input value="{{$lugat->suz}}" type="text" name="suz" class="form-control @error('suz') is-invalid @enderror">
                                @error('suz')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div>
                                <label for="exampleFormControlTextarea1" class="form-label">Ma`nosi</label>
                                <textarea class="form-control @error('mano') is-invalid @enderror" name="mano" id="exampleFormControlTextarea1" rows="3">{{$lugat->mano}}</textarea>
                                @error('mano')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Yangilash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
