@extends('admin.layouts.master')
@section('title', 'Maqolani tahrirlash')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('ilmiy.update', $ilmiy)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Maqolani tahrirlash</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Maqola nomi</div>
                                <input type="text" value="{{$ilmiy->name}}" name="ilmiy_name" class="form-control @error('ilmiy_name') is-invalid @enderror">
                                @error('ilmiy_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Maqola fayli</div>
                                <input type="file" name="ilmiy_file" class="form-control @error('ilmiy_file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                <span>{{$ilmiy->fayl}}</span>
                                @error('ilmiy_file')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Maqola turi</div>
                                <input type="text" value="{{$ilmiy->turi}}" name="ilmiy_type" class="form-control @error('ilmiy_type') is-invalid @enderror">
                                @error('ilmiy_type')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div>
                                <label for="exampleFormControlTextarea1" class="form-label">Maqola qisqacha izohi</label>
                                <textarea class="form-control @error('ilmiy_desc') is-invalid @enderror" name="ilmiy_desc" id="exampleFormControlTextarea1" rows="3">{{$ilmiy->izoh}}</textarea>
                                @error('ilmiy_desc')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Yangilash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
