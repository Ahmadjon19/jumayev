@extends('admin.layouts.master')
@section('title', 'Maqola qo\'shish')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('ilmiy.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Maqola qo'shish</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Maqola nomi</div>
                                <input type="text" name="ilmiy_name" class="form-control @error('ilmiy_name') is-invalid @enderror">
                                @error('ilmiy_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Maqola fayli</div>
                                <input type="file" name="ilmiy_file" class="form-control @error('ilmiy_file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                @error('ilmiy_file')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Maqola turi</div>
                                <input type="text" name="ilmiy_type" class="form-control @error('ilmiy_type') is-invalid @enderror">
                                @error('ilmiy_type')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div>
                                <label for="exampleFormControlTextarea1" class="form-label">Maqola qisqacha izohi</label>
                                <textarea class="form-control @error('ilmiy_desc') is-invalid @enderror" name="ilmiy_desc" id="exampleFormControlTextarea1" rows="3"></textarea>
                                @error('ilmiy_desc')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Saqlash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
