@extends('admin.layouts.master')
@section('title', 'Maqolalarni ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha maqolalar</h5>
                <a href="{{route('ilmiy.create')}}" class="btn btn-primary">Maqola qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Maqola nomi</th>
{{--                            <th>Maqola file</th>--}}
                            <th>Maqola turi</th>
                            <th>Maqola izohi</th>
{{--                            <th>Qachon qo'shilgan</th>--}}
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ilmiys as $key=>$ilmiy)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$ilmiy->name}}</td>--}}
                                <td><a href="{{asset('admin/'.$ilmiy->turi.'/'.$ilmiy->fayl)}}">{{$ilmiy->name}}</a></td>
                                <td>{{$ilmiy->turi}}</td>
                                <td>{{$ilmiy->izoh}}</td>
{{--                                <td>{{$ilmiy->created_at}}</td>--}}
                                <td class="d-flex" >
                                    <a style="height: 34px; width:66px; padding: 10px; line-height: normal; margin-top: 21px; margin-right: 5px"  href="{{route('ilmiy.edit', $ilmiy)}}" class="btn btn-success">Tahrir</a>
                                    <form action="{{route('ilmiy.destroy', $ilmiy)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete maqola?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
