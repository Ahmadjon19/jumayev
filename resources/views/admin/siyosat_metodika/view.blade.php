@extends('admin.layouts.master')
@section('title', 'Metodikalarni ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha metodikalar</h5>
                <a href="{{route('siyosat_metodika.create')}}" class="btn btn-primary">Metodika qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Metodika nomi</th>
{{--                            <th>Metodika fayli</th>--}}
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($siyosat_metodikas as $key=>$siyosat_metodika)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$siyosat_metodika->name}}</td>--}}
                                <td><a href="{{asset('admin/siyosat_metodika/'.$siyosat_metodika->file)}}">{{$siyosat_metodika->name}}</a></td>
                                <td>{{$siyosat_metodika->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('siyosat_metodika.edit', $siyosat_metodika)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('siyosat_metodika.destroy', $siyosat_metodika)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete metodika?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
