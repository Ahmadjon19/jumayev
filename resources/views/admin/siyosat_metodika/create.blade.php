@extends('admin.layouts.master')
@section('title', 'Metodika qo\'shish')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('siyosat_metodika.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Metodika qo'shish</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Metodika nomi</div>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Metodika fayli</div>
                                <input type="file" name="file" class="form-control @error('file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                @error('file')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Saqlash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
