@extends('admin.layouts.master')
@section('title', 'Akademik dastur ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Akademik dasturlarni ko'rish</h5>
                <a href="{{route('akademik.create')}}" class="btn btn-primary">Akademik dasturlarni qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Akademik dastur nomi</th>
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($akademiks as $key=>$akademik)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><a href="{{asset('admin/akademik dasturlar/'.$akademik->file)}}">{{$akademik->name}}</a></td>
                                <td>{{$akademik->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('akademik.edit', $akademik)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('akademik.destroy', $akademik)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete akademik dastur?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
