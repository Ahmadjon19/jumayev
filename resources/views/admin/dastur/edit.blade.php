@extends('admin.layouts.master')
@section('title', 'Akademik dastur tahrirlash')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('akademik.update', $akademik)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Akademik dasturni tahrirlash</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Dastur nomi</div>
                                <input value="{{$akademik->name}}" type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Dastur fayli</div>
                                <input type="file" name="file" class="form-control @error('file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                <span>{{$akademik->file}}</span>
                                @error('file')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Yangilash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
