@extends('admin.layouts.master')
@section('title', 'Farmonlarni ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha farmonlar</h5>
                <a href="{{route('farmon.create')}}" class="btn btn-primary">Farmon qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Farmon nomi</th>
{{--                            <th>Farmon file</th>--}}
                            <th>Farmon linki</th>
{{--                            <th>Created date</th>--}}
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($farmons as $key=>$farmon)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$farmon->name}}</td>
{{--                                <td><a href="{{asset('admin/'.$farmon->turi.'/'.$farmon->fayl)}}">{{$farmon->fayl}}</a></td>--}}
                                <td>{{$farmon->turi}}</td>
{{--                                <td>{{$farmon->created_at}}</td>--}}
                                <td class="d-flex align-items-center justify-content-center">
                                    <a style="max-height: 40px; margin-top: 20px" href="{{route('farmon.edit', $farmon)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('farmon.destroy', $farmon)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete farmon?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
