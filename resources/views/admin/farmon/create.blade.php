@extends('admin.layouts.master')
@section('title', 'Farmon qo\'shish')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('farmon.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Farmon qo'shish</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Farmon nomi</div>
                                <input type="text" name="farmon_name" class="form-control @error('farmon_name') is-invalid @enderror">
                                @error('farmon_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

{{--                            <div class="mb-2">--}}
{{--                                <div class="col-form-label">Farmon fayli</div>--}}
{{--                                <input type="file" name="farmon_file" class="form-control @error('farmon_file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">--}}
{{--                                @error('farmon_file')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}

                            <div class="mb-2">
                                <div class="col-form-label">Farmon linki</div>
                                <input type="text" name="farmon_type" class="form-control @error('farmon_type') is-invalid @enderror">
                                @error('farmon_type')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Saqlash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
