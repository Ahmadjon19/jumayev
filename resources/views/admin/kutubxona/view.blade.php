@extends('admin.layouts.master')
@section('title', 'Barcha kitoblar')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha kitoblar</h5>
                <a href="{{route('kutubxona.create')}}" class="btn btn-primary">Kitob qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Kitob nomi</th>
{{--                            <th>Kitob file</th>--}}
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kutubxonas as $key=>$kutubxona)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$kutubxona->name}}</td>--}}
                                <td><a href="{{asset('admin/kutubxona/'.$kutubxona->file)}}">{{$kutubxona->name}}</a></td>
                                <td>{{$kutubxona->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('kutubxona.edit', $kutubxona)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('kutubxona.destroy', $kutubxona)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete kitob?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
