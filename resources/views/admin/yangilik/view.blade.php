@extends('admin.layouts.master')
@section('title', 'Barcha yangiliklarlar')
@section('content')

    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha yangiliklar</h5>
                <a href="{{route('yangilik.create')}}" class="btn btn-primary">Yangilik qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Mavzu</th>
                            <th>Yangilik izohi</th>
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fotos as $key=>$foto)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$foto->name}}</td>
                                <td>{{$foto->matn}}</td>
                                <td>{{$foto->created_at}}</td>
                                <td class="d-flex" >
                                    <a  href="{{route('yangilik.edit', $foto)}}" class="btn btn-success">Tahrirlash</a>&nbsp;&nbsp;
                                    <a  href="{{route('yangilik.show', $foto)}}" class="btn btn-success">Ko'rish</a>
                                    <form action="{{route('yangilik.destroy', $foto)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete yangilik?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
