@extends('admin.layouts.master')
@section('title', 'Savol qo\'shish')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('savol-javob.store')}}" method="post">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Savol qo'shish</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Savol</div>
                                <input type="text" name="savol" class="form-control @error('savol') is-invalid @enderror">
                                @error('savol')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Javob</div>
                                <input type="text" name="javob" class="form-control @error('javob') is-invalid @enderror">
                                @error('javob')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Saqlash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
