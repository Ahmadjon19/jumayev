@extends('admin.layouts.master')
@section('title', 'Barcha savollar')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha savollar</h5>
                <a href="{{route('savol-javob.create')}}" class="btn btn-primary">Savol qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Savol</th>
                            <th>Javob</th>
                            <th>Qachon yaratilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($savols as $key=>$savol)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$savol->savol}}</td>
                                <td>{{$savol->javob}}</td>
                                <td>{{$savol->created_at}}</td>
                                <td class="d-flex align-items-end">
                                    <a style="max-height: 40px" href="{{route('savol-javob.edit', $savol)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('savol-javob.destroy', $savol)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete savol?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
