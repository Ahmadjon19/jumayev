@extends('admin.layouts.master')
@section('title', 'Edit user info')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-xl-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            <h5>Update {{\Illuminate\Support\Facades\Auth::user()->name." ".\Illuminate\Support\Facades\Auth::user()->lname}} info</h5>
                        </div>
                        <div class="card-body">
                            <form class="theme-form" action="{{route('dashboard.update', \Illuminate\Support\Facades\Auth::user()->id)}}" method="post">
                                @csrf
                                @method('PUT')
                                <div class="mb-3">
                                    <label class="col-form-label pt-0" for="exampleInputEmail1">Name</label>
                                    <input value="{{\Illuminate\Support\Facades\Auth::user()->name}}" name="name" class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" data-bs-original-title="" title="">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label pt-0" for="exampleInputEmail1">Last Name</label>
                                    <input value="{{\Illuminate\Support\Facades\Auth::user()->lname}}" name="lname" class="form-control @error('lname') is-invalid @enderror" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" data-bs-original-title="" title="">
                                    @error('lname')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label pt-0" for="exampleInputEmail1">Email address</label>
                                    <input value="{{\Illuminate\Support\Facades\Auth::user()->email}}" name="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" data-bs-original-title="" title="">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label pt-0" for="exampleInputPassword1">Old Password</label>
                                    <input name="old_password" class="form-control @error('old_password') is-invalid @enderror" id="exampleInputPassword1" type="password" placeholder="Old Password" data-bs-original-title="" title="">
                                    @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label pt-0" for="exampleInputPassword1">New Password</label>
                                    <input name="password" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword1" type="password" placeholder="New Password" data-bs-original-title="" title="">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label pt-0" for="exampleInputPassword1">Password confirm</label>
                                    <input name="password_confirmation" class="form-control" id="exampleInputPassword1" type="password" placeholder="Password confirm" data-bs-original-title="" title="">
                                </div>
                                <div class="card-footer text-start">
                                    <button type="submit" class="btn btn-primary" data-bs-original-title="" title="">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
