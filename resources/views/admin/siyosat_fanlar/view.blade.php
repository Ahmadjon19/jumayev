@extends('admin.layouts.master')
@section('title', 'Fanga oid hujjatlarni ko\'rish')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Fanga oid hujjatlarni ko'rish</h5>
                <a href="{{route('siyosat_fanlar.create')}}" class="btn btn-primary">Hujjat qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Kitob nomi</th>
{{--                            <th>Kitob file</th>--}}
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($siyosat_fanlars as $key=>$siyosat_fanlar)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$siyosat_fanlar->name}}</td>--}}
                                <td><a href="{{asset('admin/siyosat_fanlar/'.$siyosat_fanlar->file)}}">{{$siyosat_fanlar->name}}</a></td>
                                <td>{{$siyosat_fanlar->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('siyosat_fanlar.edit', $siyosat_fanlar)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('siyosat_fanlar.destroy', $siyosat_fanlar)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete kitob?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
