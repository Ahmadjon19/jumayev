<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="#"><img class="img-fluid for-light" src="{{asset('admin/assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="{{'admin/assets/images/logo/logo_dark.png'}}" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="#"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar" style="text-transform: none!important;">
                    <li class="back-btn"><a href="#"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6 class="lan-1">Moduls</h6>
                            <p class="lan-2">Dashboards,widgets & layout.</p>
                        </div>
                    </li>

{{--                                        Sayt haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Sayt haqida</span></a>
                        <ul class="sidebar-submenu {{request()->routeIs('dashboard/category/create') || request()->is('dashboard/category') || request()->is('dashboard/role/*/category') ? 'd-block': ''}}">
                            <li><a class=""  href="{{route('create.sayt')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('view.sayt')}}">Ko'rish</a></li>
                            <li><a class=""  href="{{route('edit.sayt')}}">Tahrirlash</a></li>
                        </ul>
                    </li>

                    {{--                                        Muallif haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Muallif haqida</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('create.muallif')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('view.muallif')}}">Ko'rish</a></li>
                            <li><a class=""  href="{{route('edit.muallif')}}">Tahrirlash</a></li>
                        </ul>
                    </li>

{{--                                                            Farmon haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Farmon va qarorlar</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('farmon.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('farmon.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            Hujjat haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Hujjatlar</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('hujjat.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('hujjat.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            Ilmiy haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Maqolalar</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('ilmiy.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('ilmiy.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            dastur haqida--}}
{{--                    <li class="sidebar-list">--}}
{{--                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Akademik dasturlar</span></a>--}}
{{--                        <ul class="sidebar-submenu">--}}
{{--                            <li><a class=""  href="{{route('akademik.create')}}">Yaratish</a></li>--}}
{{--                            <li><a class=""  href="{{route('akademik.index')}}">Ko'rish</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}

{{--                                                            lug'at haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Lug'at qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('lugat.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('lugat.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            konferensiya haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Konferensiya qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('konferensiya.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('konferensiya.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            akademik dastur haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Akademik dastur qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('akademik.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('akademik.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            dastur katalogi haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Dastur katalogi qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('katalog.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('katalog.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            siyosatshunoslikka kirish haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Siyosatshunoslikni qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('siyosat_kirish.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('siyosat_kirish.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            siyosatshunoslik metodikasi haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Metodika qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('siyosat_metodika.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('siyosat_metodika.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            siyosatshunoslik fanlari haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Siyosat fanlari qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('siyosat_fanlar.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('siyosat_fanlar.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            savol javob haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Savol-javob qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('savol-javob.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('savol-javob.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            kutubxona haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Kitob qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('kutubxona.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('kutubxona.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            havola haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Foydali havola qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('havola.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('havola.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            fotogalareya haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Fotogalareya qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('fotogalareya.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('fotogalareya.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            elon haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">E'lon qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('elon.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('elon.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>

{{--                                                            yangilik haqida--}}
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Yangilik qo'shish</span></a>
                        <ul class="sidebar-submenu">
                            <li><a class=""  href="{{route('yangilik.create')}}">Yaratish</a></li>
                            <li><a class=""  href="{{route('yangilik.index')}}">Ko'rish</a></li>
                        </ul>
                    </li>
{{--                    --}}{{--                    Category--}}
{{--                    <li class="sidebar-list">--}}
{{--                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="layers"></i><span class="">Categories</span></a>--}}
{{--                        <ul class="sidebar-submenu {{request()->is('dashboard/category/create') || request()->is('dashboard/category') || request()->is('dashboard/role/*/category') ? 'd-block': ''}}">--}}
{{--                            <li><a class="" style="color:{{request()->is('dashboard/category/create')? '#6600FF': ''}}" href="{{route('category.create')}}">Add category</a></li>--}}
{{--                            <li><a class="" style="color:{{request()->is('dashboard/category') || request()->is('dashboard/category/*/edit')? '#6600FF': ''}}" href="{{route('category.index')}}">View categories</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
