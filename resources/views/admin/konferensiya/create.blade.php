@extends('admin.layouts.master')
@section('title', 'Konferensiya qo\'shish')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('konferensiya.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Konferensiya qo'shish</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Konferensiya mavzusi</div>
                                <input type="text" name="mavzusi" class="form-control @error('mavzusi') is-invalid @enderror">
                                @error('mavzusi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Konferensiya fayli</div>
                                <input type="file" name="nizom" class="form-control @error('nizom') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                @error('nizom')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Konferensiya vaqti</div>
                                <input type="text" name="muddat" class="form-control @error('muddat') is-invalid @enderror">
                                @error('muddat')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Saqlash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
