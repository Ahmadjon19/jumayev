@extends('admin.layouts.master')
@section('title', 'Barcha konferensiyalar')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha konferensiyalar</h5>
                <a href="{{route('konferensiya.create')}}" class="btn btn-primary">Konferensiya qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Konferensiya mavzusi</th>
{{--                            <th>Konferensiya fayli</th>--}}
                            <th>Konferensiya muddati</th>
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($konferensiyas as $key=>$konferensiya)
                            <tr>
                                <td>{{$key+1}}</td>
{{--                                <td>{{$konferensiya->mavzusi}}</td>--}}
                                <td><a href="{{asset('admin/konferensiya/'.$konferensiya->nizom)}}">{{$konferensiya->mavzusi}}</a></td>
                                <td>{{$konferensiya->muddat}}</td>
                                <td>{{$konferensiya->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('konferensiya.edit', $konferensiya)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('konferensiya.destroy', $konferensiya)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete konferensiya?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
