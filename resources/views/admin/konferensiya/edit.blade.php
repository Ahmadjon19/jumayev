@extends('admin.layouts.master')
@section('title', 'Konferensiyani tahrirlash')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('konferensiya.update', $konferensiya)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Konferensiya tahrirlash</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Konferensiya mavzusi</div>
                                <input value="{{$konferensiya->mavzusi}}" type="text" name="mavzusi" class="form-control @error('mavzusi') is-invalid @enderror">
                                @error('mavzusi')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Konferensiya fayli</div>
                                <input type="file" name="nizom" class="form-control @error('nizom') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                                <span>{{$konferensiya->nizom}}</span>
                                @error('nizom')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Konferensiya vaqti</div>
                                <input value="{{$konferensiya->muddat}}" type="text" name="muddat" class="form-control @error('muddat') is-invalid @enderror">
                                @error('muddat')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Yangilash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
