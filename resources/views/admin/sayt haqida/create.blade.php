@extends('admin.layouts.master')
@section('title', 'Sayt yaratish')
@section('content')
    <br>
    <form action="{{route('store.sayt')}}" method="post">
        @csrf
        <div>
            <textarea class="@error('create_sayt') is-invalid @enderror" id="editor1" name="create_sayt" cols="30" rows="10">

            </textarea>
            @error('create_sayt')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
            @enderror
        </div>
        <br>
        <button class="btn btn-success" type="submit">Saqlash</button>
    </form>
@endsection
