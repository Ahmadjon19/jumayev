@extends('admin.layouts.master')
@section('title', 'Sayt tahrirlash')
@section('content')
    <form action="{{route('update.sayt')}}" method="post">
        @csrf
        <br>
        <div>
            <textarea class="@error('edit_sayt') is-invalid @enderror" id="editor1" name="edit_sayt" style="min-height: 1500px !important;">
                @foreach($sayts as $sayt)
                    {!! $sayt->about_sayt !!}
                @endforeach
            </textarea>
            @error('edit_sayt')
            <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
            @enderror
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Yangilash</button>
    </form>
@endsection
