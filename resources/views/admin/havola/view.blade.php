@extends('admin.layouts.master')
@section('title', 'Barcha havolalar')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha havolalar</h5>
                <a href="{{route('havola.create')}}" class="btn btn-primary">Havola qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Foydali havola</th>
                            <th>Qachon qo'shilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($havolas as $key=>$havola)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><a href="{{$havola->havola}}">{{$havola->name}}</a></td>
                                <td>{{$havola->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('havola.edit', $havola)}}" class="btn btn-success">Tahrirlash</a>
                                    <form action="{{route('havola.destroy', $havola)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete havola?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
