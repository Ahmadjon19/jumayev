@extends('admin.layouts.master')
@section('title', 'Havolani tahrirlash')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('havola.update', $havola)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Havolani tahrirlash</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Havola nomi</div>
                                <input value="{{$havola->name}}" type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <div class="col-form-label">Havola</div>
                                <input value="{{$havola->havola}}" type="text" name="havola" class="form-control @error('havola') is-invalid @enderror">
                                @error('havola')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="Yangilash">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
