@extends('admin.layouts.master')
@section('title', 'E\'lonni tahrirlash')
@section('content')
    <form action="{{route('elon.update', $elons)}}" method="post">
        @csrf
        @method('PUT')
        <br>
        <div>
            <textarea class="@error('edit') is-invalid @enderror" id="editor1" name="edit" style="min-height: 1500px !important;">

                    {!! $elons->elon !!}

            </textarea>
            @error('edit')
            <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
            </span>
            @enderror
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Yangilash</button>
    </form>
@endsection
