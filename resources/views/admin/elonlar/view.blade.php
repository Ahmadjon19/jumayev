@extends('admin.layouts.master')
@section('title', 'Barcha e\'lonlar')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Barcha e'lonlar</h5>
                <a href="{{route('elon.create')}}" class="btn btn-primary">E'lon qo'shish</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>E'lon</th>
                            <th>Qachon yaratilgan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($elons as $key=>$elon)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><div class="container">
                                        {!! $elon->elon !!}
                                    </div></td>
                                <td>{{$elon->created_at}}</td>
                                <td class="d-flex align-items-end" >
                                    <a href="{{route('elon.edit', $elon)}}" class="btn btn-success">Tahrirlash</a>&nbsp;&nbsp;
                                    <a href="{{route('elon.show', $elon)}}" class="btn btn-success">Ko'rish</a>
                                    <form action="{{route('elon.destroy', $elon)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-danger show_confirm">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script type="text/javascript">
        $('.show_confirm').click(function (event){
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete e'lon?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
