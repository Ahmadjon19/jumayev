@extends('admin.layouts.master')
@section('title', 'E\'lon yaratish')
@section('content')
    <br>
    <form action="{{route('elon.store')}}" method="post">
        @csrf
        <div>
            <textarea class="@error('elon') is-invalid @enderror" id="editor1" name="elon" cols="30" rows="10">

            </textarea>
            @error('elon')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
            @enderror
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Saqlash</button>
    </form>
@endsection
