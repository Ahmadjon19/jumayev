<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>@yield('title')</title>

{{--    <meta name="keywords" content="HTML5 Template" />--}}
{{--    <meta name="description" content="Riode - Ultimate eCommerce Template">--}}
{{--    <meta name="author" content="D-THEMES">--}}

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('main/images/icons/favicon.png')}}">
    <!-- Preload Font -->
    <link rel="preload" href="{{asset('main/fonts/riode.ttf?5gap68')}}" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="{{asset('main/vendor/fontawesome-free/webfonts/fa-solid-900.woff2')}}" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="{{asset('main/vendor/fontawesome-free/webfonts/fa-brands-400.woff2')}}" as="font" type="font/woff2"
          crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/animate/animate.min.css')}}">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/magnific-popup/magnific-popup.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/owl-carousel/owl.carousel.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('main/vendor/sticky-icon/stickyicon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/bootstrap/bootstrap.css')}}">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('main/css/demo9.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@stack('style')
</head>

<body class="home">

<div class="page-wrapper">
    @include('main.partials.navbar')
    <!-- End Header -->
    @yield('content')
    <!-- End of Main -->
    @include('main.partials.footer')
    <!-- End Footer -->
</div>

<!-- Plugins JS File -->
<script src="{{asset('main/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('main/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('main/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>

<script src="{{asset('main/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- Main JS File -->
<script src="{{asset('main/js/main.min.js')}}"></script>
<script src="{{asset('admin/assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/assets/js/datatable/datatables/datatable.custom.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@stack('script')
</body>

</html>
