@extends('main.layouts.master')
@section('title', 'Ta`lim')
@section('content')
    <div class="container pt-5">
        <div class="row" >
            <div class="col-xl-4 col-md-6 col-sm-12">
                <section class="mb-3" style=" border-top: 3px solid rgb(34, 102, 204);  background: #f2f2f2;">

                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button style="font-size: 15px" class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Siyosatshunoslik va malaka talablari
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <a style="font-size: 13px; font-weight: 600; letter-spacing: 1px;" href="{{ route('iframe',1) }}">Siyosatshunoslik va malaka talablari</a>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button style="font-size: 15px" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Konfirensiya va anjuman
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <a style="font-size: 13px; font-weight: 600; letter-spacing: 1px;" href="{{ route('iframe',2) }}">Konfirensiya va anjuman</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
            <div class="col-xl-8 col-md-6 col-sm-12">
                <section>
                    @if($file ==1)
                        <iframe src="{{ asset('60310100 - Siyosatshunoslik малака талаблари.pdf') }}" style="width: 100%; height: 80vh" frameborder="0"></iframe>
                    @else
                        <iframe src="{{ asset('Конференция_ва_анжуман_меню_ичида.pdf') }}" style="width: 100%; height: 80vh" frameborder="0"></iframe>
                    @endif
                </section>
            </div>
        </div>

    </div>
@endsection
