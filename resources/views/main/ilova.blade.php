@extends('main.layouts.master')
@section('title', 'Ta`lim')
@section('content')
    <div class="container">

        <div style="margin-top: 40px; position: relative;">
            <div style="position: absolute; top: 10px; left: 30px;">
                <h1 style="color: #fff;">SIYOSATShUNOSLIK TA'LIM YO‘NALISHI</h1>
                <p style="color: #fff; font-size: 20px">60310100</p>
            </div>

            <img src="{{ asset('main/images/main/photo_2023-10-05_16-36-16.jpg')  }}" style="width: 100%; height: 500px; object-fit: cover" alt="logo">

            <div style="position: absolute; right: 0; bottom: 0; width: 300px; height: auto; min-height: 180px; padding: 15px; background: rgba(159,45,32,.6)" >
                <div style="display: flex; flex-direction: column; margin-bottom: 6px">
                    <span style="color: #fff; opacity: .8; font-size: 13px">Trening darajasi</span>
                    <b style="color: #fff; font-size: 17px">bakalavr diplomi</b>
                </div>
                <div style="display: flex; flex-direction: column; gap: 1px; margin-bottom: 6px">
                    <span style="color: #fff; opacity: .8; font-size: 13px">O'qish shakli</span>
                    <b style="color: #fff; font-size: 17px" > kunduzgi va kechki ta'lim</b>
                </div>
                <div style="display: flex; flex-direction: column; margin-bottom: 6px">
                    <span style="color: #fff; opacity: .8; font-size: 13px">Kunduzgi ta'lim</span>
                    <b style="color: #fff; font-size: 17px">4 yil</b>
                </div>
                <div style="display: flex; flex-direction: column; margin-bottom: 6px">
                    <span style="color: #fff; opacity: .8; font-size: 13px">Kechki ta'lim</span>
                    <b style="color: #fff; font-size: 17px">4.5 yil</b>
                </div>
            </div>
        </div>



        <b style="font-size: 20px; margin-bottom: 10px; margin-top: 30px; display: block;">Kasbiy faoliyatining sohalari.</b>
        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">60310100-Siyosatshunoslik ta'lim yo‘nalishi – fan sohasidagi bakalavriat ta'lim yo‘nalishi bo‘lib, O‘zbekiston Respublikasi Fanlar akademiyasi va tarmoq  ilmiy-tadqiqot institutlari, davlat boshqaruvi organlari, mahalliy hokimiyat va vakillik organlari, ijtimoiy -siyosiy jarayonlarni tahlili bilan shug‘ullanuvchi aqliy markazlar, siyosiy tahlil ekspert guruhlari, siyosiy konsalting, siyosiy marketing bilan bog‘liq kasbiy sohalar majmuasini qamrab oladi.</p>

        <b style="font-size: 20px; margin-bottom: 10px;">Kasbiy faoliyatlarining ob'ektlari:</b>
        <p style="font-size: 18px; margin-bottom: 20px; text-align: justify">
O'zbekiston Respublikasi mudofaa tizimi, davlat xavfsizlik xizmati, milliy gvardiya, ichki ishlar organlari va ularning ilmiy-tadqiqot hamda aqliy markazlari;
O‘zbekiston Respublikasi Oliy Majlisi qo‘mitalari va komissiyalari;
davlat hokimiyati va mahalliy boshqaruv organlari (vazirliklar, qo‘mitalar, agentliklar, viloyatlar va tuman hokimliklari);
xalqaro tashkilotlar, xalqaro nodavlat tashkilotlar xizmatchisi, siyosiy menejment sohasi;
siyosiy texnolog, saylov kompaniyalarining menejeri, siyosatshunos-lingvistik tahlilchi, ekspert hamda jamoatchilik bilan aloqalar menejeri;
fuqarolik jamiyati institutlari;
O‘zbekiston Respublikasi Prezidenti huzuridagi Davlat xizmatlarini rivojlantirish agentligi va uning hududiy bo‘limlari;
xususiy sektorlardagi siyosiy konsalting xizmatlari;
        </p>

{{--        <a href="{{route('form')}}" style="display: block; background: rgb(34, 102, 204); color: #fff; font-size: 18px; width: fit-content; margin-bottom: 20px; padding: 10px 20px;">Konsultatsiya oling</a>--}}

        <div style="display: flex; align-items: center; gap: 10px">
            <h1 style="font-size: 20px">Konsultatsiya olish:</h1>
            <p style="color:rgb(34, 102, 204); font-weight: 600; font-size: 16px"> polit@buxdu.uz</p>
        </div>


        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" style="font-size: 20px" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Kasbiy faoliyat turlari
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Ilmiy-tadqiqot faoliyatda: </b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            emperik-siyosiy tadqiqot metodologiyasiga ega bo‘lish, bu bilimlarni nazariy va amaliy muamolarni hal qilishda qo‘llay olish;
                            Respublika va xorijda chop etilgan siyosatshunoslikning nazariy va amaliy sohalariga oid ilmiy-amaliy axborotlar, uslubiy materiallar, o‘quv adabiyotlarining ilmiy manbalarini o‘rganish;
                            soha bo‘yicha ilmiy-tadqiqot ishlarini bajarishda bevosita ishtirok etish;
                            mavzu (topshiriq) bo‘yicha ilmiy-amaliy ma'lumotlarni yig‘ish, ishlov berish, tahlil qilish va olingan ma'lumotlarni tizimlashtirishda ishtirok etish;
                            ilmiy-tadqiqot natijalarini va ishlanmalarini amaliyotga tadbiq etishda ishtirok etish.
                            ilmiy-amaliy seminarlar, konferensiyalarni tashkil etish hamda ilmiy, tahliliy ommabop nashrlarda maqolalar bilan qatnashish;
                            zamonaviy siyosiy tahlil usullari va texnologiyalarini amaliy jarayonlarda qo‘llash;
                            zamonaviy siyosiy jarayonlarda dolzarb bo‘lgan tadqiqot natijalaridan xabardor bo‘lish, ularni milliy manfaatlar nuqtai nazaridan qo‘llay olish;
                            ilmiy asoslangan tizimlarni tavsiya etish va foydalanish;
                            siyosiy fanlar sohasiga oid ilmiy-tadqiqot ishlarini tashkil etish hamda ilg‘or ilmiy-tadqiqot natijalarini va ishlanmalarini amaliyotga tadbiq
                            etishda qatnashish bo‘yicha faoliyat olib boorish qobiliyatlariga ega bo‘lishi lozim.

                        </p>

                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Davlat-fuqarolik xizmatlarini ko‘rsatish faoliyatida:</b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            fuqarolik jamiyatining dolzarb masalalarini bilish;
                            dunyoqarash bilan bog‘liq zamonaviy bilimlarga tizimli ega bo‘lishi, mustaqil tahlil qila olishi, kasbiy faoliyatida ulardan samarali foydalana olishi;
                            o‘z vatani tarixini bilishi, milliy va umuminsoniy qadriyatlar masalalari yuzasidan o‘z fikrini bayon qila olishi hamda ilmiy asoslay bilishi;
                            sohaga oid innovasiyalarni amaliyotga tatbiq qilishni tashkil etishi;
                            jamoa ishini tashkil qila olish qobiliyati va fazilatlarini o‘zlashtirishi;
                            kasbga oid muammolarning yechimlarini amaliyotga tatbiq etish;
                            fikrlar xilma-xil bo‘lgan sharoitda boshqaruv qarorini qabiul qilish;
                            nazorat qilish va amalga oshirilgan ishlarning natijalarini baholash;
                            mehnat jarayonida xavfsizlikni ta'minlash bo‘yicha metodik tadbirlarni ishlab chiqish va amalga oshirish qobiliyatiga ega bo‘lishi;
                            tashkilot, korxona,davlat idoralarini strategik rivojlantirishda xorijiy va milliy siyosiy bilimlarni boshqaruv tizimiga qo‘llagan holda boshqarishni tashkil etish;
                            davlat xizmatchilarini tayyorlash va malakasini oshirishda kompetensiyalarga asoslangan ta'lim (competence-based learning) modellarini joriy eta olish;
                            davlat xizmati faoliyatining samaradorligini baholash tizimini tatbiq qilish;
                            davlat organi tarkibiy tuzilmasi 9idoraviy mansub va hududiy tashkilot)ga
                            yuklatilgan aniq ko‘rsatkichlar ijrosini ta'minlash darajasini baholash;
                            xodimlarning professional tayyorgarlik darajasini oshirib boorish, ish faoliyatiga innovasiyalarni qo‘llash, xorijdagi siyosiy fanlar yutuqlarini ish
                            jarayoniga olib kirish, mehnat resurslaridan rasional foydalanish, mehnatni rag‘batlantirishning KPI (samaradorlikni baholash mezonlari) yangicha
                            usullarni boshqaruv jarayoniga olib kirish;
                            mehnat faoliyatida “raqamli boshqaruv” jarayonini qo‘llash, zamonaviy boshqaruv usullari hamda jamoa bilan ishlash tizimini tashkil etish, xodimlarni rag‘batlantirish jarayonini oshirib borishning yangicha
                            kreativ usullarini boshqaruv jarayonlariga  tatbiq etish;
                            davlat xizmatlari sifati va ulardan foydalanish imkoniyatini oshirish hamda davlat xizmatchilari faoliyatini davlat xizmatlari iste'molchisi sifatida aholiga qaratishning zamonaviy mexanizmlarini joriy etish

                        </p>

                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Tashkiliy-boshqaruv faoliyatida:</b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            inson kapitalini rivojlantirish, uzluksiz ta'lim, tashkilotning kadrlar potensiali va personalini baholash, time-management (vaqtdan samarali foydalanish),
                            yagona milliy mehnat tizimida xodimlar ma'lumotlari bilan ishlash;
                            o‘z professional, nazariy va amaliy bilimlarini uzluksiz oshirib borishi, dunyoqarashi, bilim va tajribasini domiy ravishda kengaytirishi, yangi bilimlarni
                            o‘zlashtirishga intilish, axborot-kommunikasiya texnologiyalaridan samarali foydalangan holda yangi axborotlarni olish, ilg‘or tajribalarni o‘rganish;
                            aholi bilan muloqot qilish madaniyatiga ega bo‘lish, jamoat joylarida o‘zini munosib tutish, shaxsiy xulq atvorini nazorat qilib borish;
                            o‘zining va xodimlarning salomatligi uchun qayg‘urish, qo‘l ostidagilarga g‘amxo‘rlik qilish va ularni ruhlantira olish, namunali ish olib borayotganlarni rag‘batlantirish;

                        </p>

                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Axborot-tahliliy faoliyatda:</b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            boshqaruv qarorlarini qabul qilish uchun tashkilotning tashqi va ichki muhiti  omillari to‘g‘risidagi ma'lumotlarni  to‘plash, qayta ishlash va tahlil qilish;
                            qaror qabul qilish, faoliyatni rejalashtirish va boshqarish uchun ma'lumot
                            to‘plash, tashkilotning ichki axborot tizimini yaratish va uning ishlashini
                            boshqarish;
                            tashkilotning ichki hujjat aylanish tizimini ishlab chiqish va boshqarish, tashkilotlar faoliyatining turli ko‘rsatkichlari bo‘yicha  ma'lumotlar bazasini
                            yuritish;
                            loyihalar samaradorligini baholash;
                            axborot-tahlil faoliyati natijalari bo‘yicha hisobot tayyorlash;
                            boshqaruv qarorlarining samaradorligini baholash.

                        </p>

                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Harbiy faoliyatda:</b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            O‘zbekiston Respublikasining harbiy siyosatining mazmun-mohiyatini bilish, uning o‘ziga xos  xususiyatlarini tizimli tahlil etish;
                            xavfsizlik va strategik tadqiqotlarning nazariy  va amaliy ahamiyati borasidagi konseptual bilimlarni o‘z kasbiy faoliyatida qo‘llay bilish;
                            mintaqa va xalqaro miqyosdagi harbiy tadqiqotlar faoliyati haqida nazariy bilimlarni egallash;
                            xalqaro miqyosdagi harbiy majoralarning tabiatini aniqlash va kasbiy faoliyatida milliy manfaatlar nuqtai nazaridan tahlil etish;
                            ichki va tashqi siyosatda yuzaga kelish mumkin  bo‘lgan xavf va tahdidlarni baholash va ularning ko‘lamini tizimli tahlil  etish ko‘nikmasini shakllantirish orqali
                            o‘z-o‘zini takomillashtirib borish
                        </p>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" style="font-size: 20px" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Kasbiy kompetensiyalarga qo‘yiladigan talablar
                    </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Kasbiy kompetensiyalarga qo‘yiladigan talablar:</b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            davlat siyosatining dolzarb masalalarini bilishi, ijtimoiy-iqtisodiy muammolar va jarayonlarni mustaqil tahlil qila olish;
                            xorijiy tillardan birida kasbiy faoliyatiga doir hujjatlar va ishlar mohiyatini tushunishi, tabiiy ilmiy fanlar bo‘yicha kasbiy faoliyati doirasida zaruriy bilimlarga ega bo‘lishi
                            hamda ulardan zamonaviy ilmiy asoda kasb faoliyatida foydalana bilish;
                            axborot texnologiyalarini kasbiy faoliyatida qo‘llay bilishi, axborotlarni yig‘ish, saqlash,qayta ishlash va ulardan foydalanish usullarini egallagan bo‘lishi,
                            faoliyatida mustaqil asoslangan qarorlar qabul qila olish;
                            yangi bilimlarni mustaqil egallay bilishi, o‘z ustida ishlashi va mehnat qila olish;
                            sog‘lom turmush tarzi va unga amal qilish zarurati to‘g‘risida tasavvurga ega bo‘lish.
                            ta'lim yo‘nalishi ixtisoslik fanlarini o‘rganish va chuqur egallash uchun zarur bo‘lgan fundamental umumkasbiy bilimlarni, amaliy ko‘nikma va o‘quvlarni shakllantirish;
                            ta'lim yo‘nalishiga muvofiq kasbiy faoliyati sohalarida erishilgan asosiy yutuqlar, muammolar va ularning rivojlanish istiqbollari haqida tasavvur hosil qilishni;
                            tegishli bakalavriat yo‘nalishi doirasida tanlangan mutaxassislik bo‘yicha magistraturada oliy ta'limni davom ettirish;
                            xorijiy tillardan birida kasbiy faoliyatga oid hujjatlar va ishlar mohiyatini tushunish va kasbiy faoliyati uchun yetarli darajada undan foydalana olishi;
                            olingan kasbiy tajribani tanqidiy ko‘rib chiqish, o‘z-o‘zini rivojlantirish, malaka oshirish va o‘z kasbiy faoliyatining turi hamda xarakterini o‘zgartirishga qodir bo‘lishi;
                            axborotlarni yig‘ish, saqlash, qayta ishlash va ulardan foydalanish usullarini bilish va faoliyatida mustaqil asoslangan qarorlar qabul qila olishni;
                            siyosatshunoslik sohasini ilmiy-texnologik anglash orqali, uning nazariy, emperik, fundamental va amaliy asoslarini qo‘llashni bilishni;
                            amaliy siyosatshunoslikning tadqiqot usullari, bosqichlari va tarkibiy qismlari, saylov kampaniyalarini o‘tkazish texnologiyalarini bilish va qo‘llay olish;
                            siyosiy jarayonlarning vujudga kelishi, bosqichlari, va shakllarini hamda siyosiy jarayonlarni tizimli-funksional tahlilini amalga oshirishni;
                            dunyoda kechayotgan turli siyosiy voqea-hodisalar va jarayonlarning mazmun-mohiyatini chuqur hamda ishonchli manbalar asosida o‘rganish, shuningdek, mazkur voqea-hodisalar va jarayonlarni tahlil etish,
                            tegishli xulosa chiqarish hamda ularning kelgusi holatiga oid bashorat qilish borasida bilim va ko‘nikmani amalda qo‘llay olishni;
                            xalqaro munosabatlarning rivojlanish qonuniyatlari, asoslari va tamoyillarini,
                            zamonaviy xalqaro munosabatlarda milliy va strategik manfaatlarni hisobga ola bilishni;
                            Sharq va G‘arb mutafakkirlarining qarashlarida siyosiy hokimiyatning legitimligi va legallik darajalarini aniqlay olishni;
                            jahon siyosatining paradigmalariga tayangan holda O‘zbekiston Respublikasining zamonaviy jahon siyosatidagi o‘rnini anglashni ;
                            zamonaviy xalqaro xavfsizlik tizimida O‘zbekiston Respublikasining ishtirokini milliy manfaatlar nuqtai nazaridan qo‘llay olishni;
                            siyosiy texnologiyalarning zamonaviy siyosiy jarayonlardagi tutgan o‘rnini baholash hamda tashkiliy  boshqaruvda  foydalana olishni;
                            siyosatshunoslikning metodologik usullarini amaliyotda qo‘llay olish;
                            siyosiy konfliktlarni anglash va baholash hamda yechimin topish usullarini kasbiy faoliyatda foydalana bilish;
                            qiyosiy siyosatshunoslikning usullari, qiyosiy siyosatshunoslikda “keys” “field research” usullarini davlat va jamiyatga xos siyosiy madaniyatni qiyoslash
                            jarayonlariga tadbiq etish borasida ilmiy bilimlar, amaliy mahorat, va ko‘nikmalarga ega bo‘lishi kerak.
                        </p>


                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Amaliyotga qo‘yiladigan talablar.</b>
                        <p style="font-size: 18px; margin-bottom: 20px;text-align: justify">
                            Malaka amaliyot – umumkasbiy va ixtisoslik fanlaridan nazariy bilimlarni mustahkamlash va amaliy (ishlab chiqarish) jarayonlari bilan uyg‘unlashtirish,
                            tegishli amaliy ko‘nikmalar, kompetensiyalar va malakalarni
                            shakllantirishga qaratiladi.
                            Ta'lim yo‘nalishi bo‘yicha quyidagi amaliyotlar o‘tkaziladi:
                            1. O‘quv tanishuv amaliyoti;
                            2. Ishlab chiqarish amaliyoti;
                            3. Bitiruv oldi amaliyoti.

                        </p>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" style="font-size: 20px" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Asosiy o'quv kurslari
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <b style="color: #9f2d20; font-size: 20px; margin-bottom: 10px;">Fanlar katalogining tuzilishi:</b>
                        <table style="border-collapse: collapse;border: none;width: 100%;">
                            <tbody>
                            <tr>
                                <td style="width:42.3pt;border:solid windowtext 1.0pt;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>T.r</span></strong></p>
                                </td>
                                <td style="width:77.95pt;border:solid windowtext 1.0pt;border-left:  none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>Fanning malakaviy</span></strong></p>
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>kodi</span></strong></p>
                                </td>
                                <td style="width:177.2pt;border:solid windowtext 1.0pt;border-left:  none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>O&lsquo;quv fanlari, bloklar va faoliyat turlari</span></strong></p>
                                </td>
                                <td style="width:70.85pt;border:solid windowtext 1.0pt;border-left:  none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>Umumiy yuklama</span></strong></p>
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>ning hajmi soatlarda</span></strong></p>
                                </td>
                                <td style="width:56.7pt;border:solid windowtext 1.0pt;border-left:  none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>Kredit miqdori</span></strong></p>
                                </td>
                                <td style="width:49.3pt;border:solid windowtext 1.0pt;border-left:  none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>Semestri</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.00</span></strong></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>Majburiy fanlar</span></strong></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>5040</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>168</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.01&nbsp;</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>O&lsquo;YT1104</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>O&lsquo;zbekistonning eng yangi tarixi</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.02</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>JMS1104</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Jismoniy madaniyat va sport</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.03</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>FAL1204</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Falsafa&nbsp;</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>2</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.04</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>O&lsquo;RT1204</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>O&lsquo;zbek (rus) tili</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>2</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.05</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>XT1104</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Xorijiy &nbsp;til</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.06</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>DIN1404</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Dinshunoslik</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.07</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>KOH1106</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Konstitutsiyaviy huquq</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.08</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>STT1112</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy ta&rsquo;limotlar tarixi</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>360</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>12</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1,2</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.09</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SIK1112</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosatshunoslikka kirish</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>360</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>12</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1,2</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.10</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SMA1204</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy madaniyat, mafkura va antropologiya</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>2</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.11</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>KRA1204</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Kratologiya&nbsp;</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>4</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>2</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.12</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SIM1306</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy menejment</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>3</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.13</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>GEO1310</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Geosiyosat</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>300</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>10</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>3,4</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.14</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>AMS13</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Amaliy siyosatshunoslik</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>300</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>10</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>3,4</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.15</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>DAB1310</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Davlat boshqaruvi</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>300</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>10</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>3,4</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.16</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SJT1310</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy jarayonlarining tizimli tahlili</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>300</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>10</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>3,4</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-top: none;border-left: 1pt solid windowtext;border-bottom: none;border-right: 1pt solid windowtext;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.17</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>JAS1308</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Jahon siyosati</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>240</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>8</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>3,4</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border: 1pt solid windowtext;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.18</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: 1pt solid windowtext;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-image: initial;border-left: none;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>XAM1512</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: 1pt solid windowtext;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-image: initial;border-left: none;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Xalqaro munosabatlar</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: 1pt solid windowtext;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-image: initial;border-left: none;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>360</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: 1pt solid windowtext;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-image: initial;border-left: none;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>12</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: 1pt solid windowtext;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-image: initial;border-left: none;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>5,6</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.19</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SIT1510</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy texnologiyalar</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>300</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>10</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>5,6</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.20</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>XAA1506</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Xavfsizlik asoslari</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>5</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.21</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SIK1606</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy konsalting</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.22</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SIK1706</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy konfliktologiya</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>7</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.23</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>SIT1706</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Siyosiy tahlil</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>7</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>1.24</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>ZIS1706</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Zamonaviy iqtisodiy siyosat</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>180</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>7</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>2.00</span></strong></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>Tanlov fanlar&nbsp;</span></strong></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>1080</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>36</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>5,6,7</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;color:black;'>2.00</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Tanlov fanlar</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>1080</span></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>36</span></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 120.25pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>Klassifikatsiya</span></strong></p>
                                </td>
                                <td colspan="4" style="width: 354.05pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;line-height:  normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>siyosatshunos, siyosiy tahlilchi&nbsp;</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:right;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>Jami</span></strong></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>6120</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>204</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td colspan="2" style="width: 255.15pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>Malaka amaliyoti*</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>840</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>28</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>2,4,6,8</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td colspan="2" style="width: 255.15pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>BMI (Yakuniy davlat attestatsiya)</span></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>240</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>8</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:right;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>Jami&nbsp;</span></strong></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>1080</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>36</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 42.3pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(222, 234, 246);padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 77.95pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:  justify;line-height:normal;'><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></p>
                                </td>
                                <td style="width: 177.2pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:right;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>Hammasi</span></strong></p>
                                </td>
                                <td style="width: 70.85pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>7200</span></strong></p>
                                </td>
                                <td style="width: 56.7pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>240</span></strong></p>
                                </td>
                                <td style="width: 49.3pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0in 5.4pt;vertical-align: top;">
                                    <p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;font-size:11.0pt;font-family:"Calibri",sans-serif;text-align:center;line-height:normal;'><strong><span style='font-size:16px;font-family:"Arial",sans-serif;'>&nbsp;</span></strong></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>







    </div>
@endsection
