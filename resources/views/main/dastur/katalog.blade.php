@extends('main.layouts.master')
@section('title', 'Dastur kataloglari')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Dastur kataloglari</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Dastur katalogi nomi</th>
                                <th>Dastur katalogi fayli</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($katalogs as $key=>$katalog)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$katalog->name}}</td>
                                    <td><a href="{{asset('admin/dastur katalogi/'.$katalog->file)}}">{{$katalog->file}}</a></td>
{{--                                    <td>{{$katalog->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

