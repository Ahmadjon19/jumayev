@extends('main.layouts.master')
@section('title', 'Akademik dasturlar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Akademik dasturlar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Dastur nomi</th>
                                <th>Dastur fayli</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($akademiks as $key=>$akademik)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$akademik->name}}</td>
                                    <td><a href="{{asset('admin/akademik dasturlar/'.$akademik->file)}}">{{$akademik->file}}</a></td>
{{--                                    <td>{{$akademik->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

