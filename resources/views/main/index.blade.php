@extends('main.layouts.master')
@section('title', 'FPR.Buxdu.uz')
@section('content')
    <main class="main mt-lg-4">
        <div class="page-content">
            <section class="intro-section">
                <div
                    class="owl-carousel owl-theme row owl-nav-bg owl-dot-inner owl-dot-white owl-nav-fade intro-slider animation-slider cols-1 gutter-no"
                    data-owl-options="{
                        'nav': false,
                        'dots': true,
                        'loop': true,
                        'items': 1,

                        'responsive': {
                            '992': {
                                'nav': true,
                                'dots': false
                            }
                        }
                    }">
                                        <div class="banner banner-fixed intro-slide1" style="background-color: #edecec">
                                            <figure>
                                                <img src="{{asset('main/images/main/photo_2023-10-05_16-36-16.jpg')}}" alt="intro-banner" width="1903"
                                                     height="514" style="width: 100%; height: 514px"/>
                                            </figure>

                                        </div>
                                        <div class="banner banner-fixed intro-slide2" style="background-color: #edecec">
                                            <figure>
                                                <img src="{{asset('main/images/main/1.jpg')}}" alt="intro-banner" width="1903"
                                                     height="514" style="width: 100%; height: 514px" />
                                            </figure>

                                        </div>
                                        <div class="banner banner-fixed intro-slide3" style="background-color: #ed711b">
                                            <figure>
                                                <img src="{{asset('main/images/main/international-marketing_1.jpg')}}" alt="intro-banner" width="1903"
                                                     height="514" style="width: 100%; height: 514px" />
                                            </figure>

                                        </div>


                </div>
            </section>
        </div>

    </main>
@endsection
