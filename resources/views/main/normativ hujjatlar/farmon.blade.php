@extends('main.layouts.master')
@section('title', 'Farmonlar va qarorlar')
@section('content')
    <style>
        .button_hover:hover{
            background-color: black;
        }
    </style>
    <br><br>
    <div class="container">
    <div class="col-sm-11 container">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Farmonlar va qarorlar</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table>
                        @foreach($farmons as $key=>$farmon)
                            <tr>
                                <td><hr></td>
                            </tr>
                            <tr>
                                <td style="width: 87%"><p style="margin: 10px 10px 10px 0;">{{$farmon->name}}</p></td>
                                <td>
                                    <a target="_blank" href="{{$farmon->turi}}" class="button_hover" style="padding: 8px 20px; background-color: #119ee6; color: #fff; border-radius: 0;font-size: 14px;line-height: 24px;font-weight: 600; border-color: #119ee6;">To'liq matn</a>

                                </td>
                            </tr>
                        @endforeach
                            <tr>
                                <td style="width: 87%"><p style="margin: 10px 10px 10px 0;">Yangi O'zbekistonning taraqqiyot strategiyasi</p></td>
                                <td>
                                    <a target="_blank" href="{{ asset('farmon/Фармон ва карор1.pdf') }}" class="button_hover" style="padding: 8px 20px; background-color: #119ee6; color: #fff; border-radius: 0;font-size: 14px;line-height: 24px;font-weight: 600; border-color: #119ee6;">To'liq matn</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 87%"><p style="margin: 10px 10px 10px 0;">Yoshlarga oid davlat siyosati</p></td>
                                <td>
                                    <a target="_blank" href="{{ asset('farmon/Фармон ва қарор 2.pdf') }}" class="button_hover" style="padding: 8px 20px; background-color: #119ee6; color: #fff; border-radius: 0;font-size: 14px;line-height: 24px;font-weight: 600; border-color: #119ee6;">To'liq matn</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 87%"><p style="margin: 10px 10px 10px 0;">Siyosiy fanlar soxasida kadrlarni tayyorlash</p></td>
                                <td>
                                    <a target="_blank" href="{{ asset('farmon/Фармон ва қарор 3.pdf') }}" class="button_hover" style="padding: 8px 20px; background-color: #119ee6; color: #fff; border-radius: 0;font-size: 14px;line-height: 24px;font-weight: 600; border-color: #119ee6;">To'liq matn</a>
                                </td>
                            </tr>
                    </table>
{{--                    <table class="display" id="basic-1">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>#</th>--}}
{{--                            <th>Hujjat nomi</th>--}}
{{--                            <th>Hujjat fayl</th>--}}
{{--                            <th>Hujjat turi</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach($farmons as $key=>$farmon)--}}
{{--                            <tr>--}}
{{--                                <td>{{$key+1}}</td>--}}
{{--                                <td>{{$farmon->name}}</td>--}}
{{--                                <td><a href="{{asset('admin/'.$farmon->turi.'/'.$farmon->fayl)}}">{{$farmon->fayl}}</a></td>--}}
{{--                                <td>{{$farmon->turi}}</td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
                </div>
            </div>
        </div>
    </div>
    <br>
    </div>
@endsection

