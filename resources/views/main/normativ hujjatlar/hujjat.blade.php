@extends('main.layouts.master')
@section('title', 'Normativ hujjatlar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Normativ hujjatlar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Hujjat nomi</th>
                                <th>Hujjat fayl</th>
                                <th>Hujjat turi</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hujjats as $key=>$hujjat)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$hujjat->hujjat_nomi}}</td>
                                    <td><a href="{{asset('admin/'.$hujjat->hujjat_turi.'/'.$hujjat->hujjat_fayl)}}">{{$hujjat->hujjat_fayl}}</a></td>
                                    <td>{{$hujjat->hujjat_turi}}</td>
{{--                                    <td>{{$hujjat->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

