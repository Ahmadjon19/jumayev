@extends('main.layouts.master')
@section('title', 'Sayt haqida')
@section('content')
    <div style="text-align: center">
        @foreach($sayts as $sayt)
            {!! $sayt->about_sayt !!}
        @endforeach
    </div>
@endsection
