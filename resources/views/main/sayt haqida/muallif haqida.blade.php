@extends('main.layouts.master')
@section('title', 'Muallif haqida')
@section('content')
    <div style="text-align: center">
        @foreach($muallifs as $muallif)
            {!! $muallif->about_muallif !!}
        @endforeach
    </div>
@endsection
