@extends('main.layouts.master')
@section('title', 'Siyosiy fanlar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Siyosiy fanlar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kitob nomi</th>
                                <th>Kitob fayli</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($siyosat_fanlars as $key=>$siyosat_fanlar)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$siyosat_fanlar->name}}</td>
                                    <td><a href="{{asset('admin/siyosat_fanlar/'.$siyosat_fanlar->file)}}">{{$siyosat_fanlar->file}}</a></td>
{{--                                    <td>{{$siyosat_fanlar->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

