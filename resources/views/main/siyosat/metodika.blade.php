@extends('main.layouts.master')
@section('title', 'Siyosatshunoslik metodikasi')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Siyosatshunoslik metodikasi</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Metodika nomi</th>
                                <th>Metodika fayli</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($siyosat_metodikas as $key=>$siyosat_metodika)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$siyosat_metodika->name}}</td>
                                    <td><a href="{{asset('admin/siyosat_metodika/'.$siyosat_metodika->file)}}">{{$siyosat_metodika->file}}</a></td>
{{--                                    <td>{{$siyosat_metodika->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

