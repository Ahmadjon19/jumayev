@extends('main.layouts.master')
@section('title', 'Yangiliklar')
@section('content')
    <div class="product-wrapper-grid">
        <div class="row">
            <div class="col-xl-3 col-sm-6 xl-4">
                @for($i=0; $i<count($fotos); $i++)
                    <div class="card">
                        <div class="product-box">
                            <div class="product-img"><img class="img-fluid" src="{{asset('admin/yangilik/'.$all_image[$i][0])}}" alt="">

                            </div>

                            <div class="product-details">
                                <h4>{{$fotos[$i]->name}}</h4>
                                <p>{{$fotos[$i]->matn}}</p>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>
@endsection
