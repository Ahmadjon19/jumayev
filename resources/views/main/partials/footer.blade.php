<br>
<footer style="background-color: #26c; color: white; " class="footer">
    <div class="container">
        <div class="footer-middle">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="widget widget-about">
                        <a href="#" class="logo-footer">
                            <img src="{{asset('main/rasm/buxdu.png')}}" alt="logo-footer" width="150"
                                 height="43" />
                        </a>
                        <div style="width: 300px !important;" class="" data-aos="fade-up">
                            <h3>Bog`lanish</h3>
                            <div style="margin-bottom: 10px;" class="link link__animation">
                                <i style="font-size: 20px" class="fa-solid fa-phone"></i>
                                <span style="font-size: 18px; margin-left: 10px">(+998) 90 612-64-31</span>
                            </div>
                            <div style="margin-bottom: 10px;" class="link link__animation">
                                <i style="font-size: 20px" class="fa-solid fa-fax"></i>
                                <span style="font-size: 18px; margin-left: 10px">(+998) 65 221-26-62</span>
                            </div>
                            <div style="margin-bottom: 10px;" class="link link__animation">
                                <i style="font-size: 20px" class="fa-solid fa-location-dot"></i>
                                <span style="font-size: 18px; margin-left: 10px">Buxoro sh. M.Iqbol ko`chasi 11-uy</span>
                            </div>
                            <div style="margin-bottom: 10px;" class="link link__animation">
                                <i style="font-size: 20px" class="fa-solid fa-envelope"></i>
                                <span style="font-size: 18px; margin-left: 10px">buxdu_prorektor@buxdu.uz</span>
                            </div>
                            <div class="link link__icons">
                                <a href="https://t.me/buxdu_uz"><i style="font-size: 20px; margin-right: 10px" class="fa-brands fa-telegram"></i></a>
                                <a href="https://www.facebook.com/buxdu"><i style="font-size: 20px; margin-right: 10px;" class="fa-brands fa-facebook"></i></a>
                                <a href="https://www.instagram.com/buxdu1/"><i style="font-size: 20px; margin-right: 10px;" class="fa-brands fa-instagram"></i></a>
                                <a href="https://www.youtube.com/c/BuxDUuzedu"><i style="font-size: 20px; margin-right: 10px;" class="fa-brands fa-youtube"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="content-col col-md-4" data-aos="fade-up">
                    <h3>Xarita</h3>
                    <div class="xarita">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3066.994842410637!2d64.42042371487301!3d39.76222420310848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f50060d1c5c0027%3A0xf76b636757c475a5!2z0JHRg9GF0LDRgNGB0LrQuNC5INCT0L7RgdGD0LTQsNGA0YHRgtCy0LXQvdC90YvQuSDQo9C90LjQstC10YDRgdC40YLQtdGC!5e0!3m2!1sru!2s!4v1667027626470!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>

            </div>
        </div>

        <!-- End Footer Middle -->
        <div class="footer-bottom">
            <div class="footer-left">
{{--                <figure class="payment">--}}
{{--                    <img src="{{asset('main/images/payment.png')}}" alt="payment" width="159" height="29" />--}}
{{--                </figure>--}}
            </div>
            <div class="footer-center">
                <p style="color: white" class="copyright">Barcha huquqlar himoyalangan</p>
            </div>
            <div class="footer-right">
{{--                <div class="social-links">--}}
{{--                    <a href="#" title="social-link" class="social-link social-facebook fab fa-facebook-f"></a>--}}
{{--                    <a href="#" title="social-link" class="social-link social-twitter fab fa-twitter"></a>--}}
{{--                    <a href="#" title="social-link" class="social-link social-linkedin fab fa-linkedin-in"></a>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
    <!-- End Footer Bottom -->
</footer>

{{--<footer>--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="content reveal active">--}}
{{--                <div class="content-col col-md-4" data-aos="fade-up">--}}
{{--                    <h3>Bog`lanish</h3>--}}
{{--                    <div class="link link__animation">--}}
{{--                        <i class="fa-solid fa-phone"></i>--}}
{{--                        <p>(+998) 65 221-29-14</p>--}}
{{--                    </div>--}}
{{--                    <div class="link link__animation">--}}
{{--                        <i class="fa-solid fa-fax"></i>--}}
{{--                        <p>8(365) 221-27-07</p>--}}
{{--                    </div>--}}
{{--                    <div class="link link__animation">--}}
{{--                        <i class="fa-solid fa-location-dot"></i>--}}
{{--                        <p>Buxoro sh. M.Iqbol ko`chasi 11-uy</p>--}}
{{--                    </div>--}}
{{--                    <div class="link link__animation">--}}
{{--                        <i class="fa-solid fa-envelope"></i>--}}
{{--                        <p>buxdu_rektor@buxdu.uz</p>--}}
{{--                    </div>--}}
{{--                    <div class="link link__icons">--}}
{{--                        <a href="https://t.me/buxdu_uz"><i class="fa-brands fa-telegram"></i></a>--}}
{{--                        <a href="https://www.facebook.com/buxdu"><i class="fa-brands fa-facebook"></i></a>--}}
{{--                        <a href="https://www.instagram.com/buxdu1/"><i class="fa-brands fa-instagram"></i></a>--}}
{{--                        <a href="https://www.youtube.com/c/BuxDUuzedu"><i class="fa-brands fa-youtube"></i></a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="content-col col-md-4" data-aos="fade-up">--}}
{{--                    <h3>Rekvizitlar</h3>--}}
{{--                    <ul>--}}
{{--                        <li>Moliya vazirligi g`aznachiligi</li>--}}
{{--                        <li>Hisob raqami: 23402000300100001010</li>--}}
{{--                        <li>MFO: 00014 INN: 201122919</li>--}}
{{--                        <li>Markaziy bank HKKM Toshkent sh.</li>--}}
{{--                        <li>(BuxDu: 400910860064017950100079002)</li>--}}
{{--                        <li>INN: 201504275</li>--}}
{{--                        <li>To`lov maqsadida FIO (unikal kod yoziladi)</li>--}}
{{--                        <li>--}}
{{--                            <a href="https://play.google.com/store/apps/details?id=uz.buxdu.buxdu">--}}
{{--                                <img src="/static/images/google_play.png" class="img-fluid" alt="logo">--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}

{{--                <div class="content-col col-md-4" data-aos="fade-up">--}}
{{--                    <h3>Xarita</h3>--}}
{{--                    <div class="xarita">--}}
{{--                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3066.994842410637!2d64.42042371487301!3d39.76222420310848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f50060d1c5c0027%3A0xf76b636757c475a5!2z0JHRg9GF0LDRgNGB0LrQuNC5INCT0L7RgdGD0LTQsNGA0YHRgtCy0LXQvdC90YvQuSDQo9C90LjQstC10YDRgdC40YLQtdGC!5e0!3m2!1sru!2s!4v1667027626470!5m2!1sru!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="row">--}}
{{--            <div class="col-md-11">--}}
{{--                <hr>--}}
{{--                <p class="text-center">Barcha huquqlar himoyalangan. © 2023 <a class="footer_link" href="/">buxdu.uz</a>--}}
{{--                </p>--}}
{{--            </div>--}}
{{--            <div class="col-md-1" id="www">--}}


{{--                <script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=G-LT5C5FCG0W&amp;cx=c&amp;_slc=1"></script><script async="" src="https://mc.yandex.ru/metrika/tag.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script language="javascript" type="text/javascript" sync="">/*<![CDATA[*/--}}
{{--                    top_js="1.0";top_r="id=30140&r="+escape(document.referrer)+"&pg="+escape(window.location.href);document.cookie="smart_top=1; path=/";top_r+="&c="+(document.cookie?"Y":"N")/*]]>*/--}}
{{--                </script>--}}
{{--                <script language="javascript1.1" type="text/javascript">/*<![CDATA[*/--}}
{{--                    top_js="1.1";top_r+="&j="+(navigator.javaEnabled()?"Y":"N")/*]]>*/--}}
{{--                </script>--}}
{{--                <script language="javascript1.2" type="text/javascript">/*<![CDATA[*/--}}
{{--                    top_js="1.2";top_r+="&wh="+screen.width+'x'+screen.height+"&px="+--}}
{{--                        (((navigator.appName.substring(0,3)==="Mic"))?screen.colorDepth:screen.pixelDepth)/*]]>*/--}}
{{--                </script>--}}

{{--                <script language="javascript1.3" type="text/javascript"> top_js="1.3";</script>--}}
{{--                <script language="JavaScript" type="text/javascript">/*<![CDATA[*/top_rat="&col=0063AF&t=ffffff&p=DD7900";top_r+="&js="+top_js+"";document.write('<a href="https://www.uz/ru/res/visitor/index?id=30140" target=_top><img src="https://cnt0.www.uz/counter/collect?'+top_r+top_rat+'" width=88 height=31 border=0 alt="??? www.uz"></a>')/*]]>*/--}}

{{--                </script><a href="https://www.uz/ru/res/visitor/index?id=30140" target="_top"><img src="https://cnt0.www.uz/counter/collect?id=30140&amp;r=&amp;pg=https%3A//buxdu.uz/&amp;c=Y&amp;j=N&amp;wh=1536x864&amp;px=24&amp;js=1.3&amp;col=0063AF&amp;t=ffffff&amp;p=DD7900" width="88" height="31" border="0" alt="??? www.uz"></a>--}}
{{--                <noscript>--}}
{{--                    <A href="https://www.uz/ru/res/visitor/index?id=30140" target=_top>--}}
{{--                        <IMG style="width:88; height:31;" height=31--}}
{{--                             src="https://cnt0.www.uz/counter/collect?id=30140&pg=https%3A//uzinfocom.uz&&col=0063AF&amp;t=ffffff&amp;p=DD7900"--}}
{{--                             width=88 border=0 alt="??? www.uz">--}}
{{--                    </A>--}}
{{--                </noscript>--}}


{{--                <!--google analytics begin-->--}}
{{--                <script>--}}
{{--                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){--}}
{{--                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),--}}
{{--                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)--}}
{{--                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');--}}

{{--                    ga('create', 'UA-31930099-1', 'buxdu.uz');--}}
{{--                    ga('send', 'pageview');--}}


{{--                </script>--}}
{{--                <!-- google analytics end -->--}}


{{--                <!-- Yandex.Metrika counter -->--}}
{{--                <script type="text/javascript">--}}
{{--                    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};--}}
{{--                        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})--}}
{{--                    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");--}}

{{--                    ym(53017267, "init", {--}}
{{--                        clickmap:true,--}}
{{--                        trackLinks:true,--}}
{{--                        accurateTrackBounce:true--}}
{{--                    });--}}

{{--                </script>--}}
{{--                <noscript>--}}
{{--                    <div><img src="https://mc.yandex.ru/watch/53017267" style="position:absolute; left:-9999px;"--}}
{{--                              alt=""/></div>--}}
{{--                </noscript>--}}
{{--                <!-- /Yandex.Metrika counter -->--}}

{{--                <!-- Top100 (Kraken) Counter -->--}}
{{--                <script>--}}
{{--                    (function (w, d, c) {--}}
{{--                        (w[c] = w[c] || []).push(function() {--}}
{{--                            var options = {--}}
{{--                                project: 6872456,--}}
{{--                            };--}}
{{--                            try {--}}
{{--                                w.top100Counter = new top100(options);--}}
{{--                            } catch(e) { }--}}
{{--                        });--}}
{{--                        var n = d.getElementsByTagName("script")[0],--}}
{{--                            s = d.createElement("script"),--}}
{{--                            f = function () { n.parentNode.insertBefore(s, n); };--}}
{{--                        s.type = "text/javascript";--}}
{{--                        s.async = true;--}}
{{--                        s.src =--}}
{{--                            (d.location.protocol == "https:" ? "https:" : "http:") +--}}
{{--                            "//st.top100.ru/top100/top100.js";--}}

{{--                        if (w.opera == "[object Opera]") {--}}
{{--                            d.addEventListener("DOMContentLoaded", f, false);--}}
{{--                        } else { f(); }--}}
{{--                    })(window, document, "_top100q");--}}
{{--                    <img src="//counter.rambler.ru/top100.cnt?pid=6872456" alt="Топ-100" />--}}

{{--                </script>--}}



{{--                <noscript>--}}
{{--                    <img src="//counter.rambler.ru/top100.cnt?pid=6872456" alt="Топ-100"/>--}}
{{--                </noscript>--}}
{{--                <!-- END Top100 (Kraken) Counter -->--}}
{{--                <!-- START WWW.UZ TOP-RATING -->--}}
{{--                <script language="javascript" type="text/javascript">--}}
{{--                    <!----}}
{{--                    top_js="1.0";top_r="id=42296&r="+escape(document.referrer)+"&pg="+escape(window.location.href);document.cookie="smart_top=1; path=/"; top_r+="&c="+(document.cookie?"Y":"N")--}}
{{--                    //-->--}}

{{--                </script>--}}
{{--                <script language="javascript1.1" type="text/javascript">--}}
{{--                    <!----}}
{{--                    top_js="1.1";top_r+="&j="+(navigator.javaEnabled()?"Y":"N")--}}
{{--                    //-->--}}

{{--                </script>--}}
{{--                <script language="javascript1.2" type="text/javascript">--}}
{{--                    <!----}}
{{--                    top_js="1.2";top_r+="&wh="+screen.width+'x'+screen.height+"&px="+--}}
{{--                        (((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth)--}}
{{--                    //-->--}}

{{--                </script>--}}
{{--                <script language="javascript1.3" type="text/javascript">--}}
{{--                    <!----}}
{{--                    top_js="1.3";--}}
{{--                    //-->--}}

{{--                </script>--}}
{{--                <!-- FINISH WWW.UZ TOP-RATING -->--}}


{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</footer>--}}
