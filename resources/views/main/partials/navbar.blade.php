<header class="header" style="margin-top: 0">
    <div class="header-top">
        <div class="container">
            <div class="header-right">

            </div>
        </div>
    </div>
    <br>

    <div style="background-color: #26c; margin-top: 0" class="header-bottom has-center sticky-header fix-top sticky-content d-lg-show">
        <div  class="container">
            <div style="background-color: #26c" class="inner-wrap">
                <div class="header-left">

                </div>
                <div class="header-center">
                    <nav class="main-nav">
                        <ul class="menu">

                            <li >
                                <a href="{{route('talim')}}">Ta'lim</a>
                            </li>

                            <li>
                                <a href="#">FPR.Buxdu.uz haqida</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="{{route('sayt_haqida')}}">Portal haqida</a></li>
                                                <hr>
                                                <li><a href="{{route('muallif_haqida')}}">Muallif haqida</a></li>
                                            </ul>
                                </div>
                            </li>

                            <li>
                                <a href="#">Normativ huquqiy hujjatlar</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="{{asset('constitution.pdf')}}">O'zbekiston Respublikasi Konstitutsiyasi</a></li>
                                                <hr>
                                                <li><a href="{{route('view.farmon')}}">Farmon va qarorlar</a></li>
                                                <hr>
                                                <li><a href="{{route('view.hujjat')}}">Idoraviy normativ-huquqiy hujjatlar</a></li>
                                            </ul>
                                </div>
                            </li>

                            <li>
                                <a href="#">Ilm va fan</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="{{route('view.tadqiqot')}}">Ilmiy tadqiqot</a></li>
                                                <hr>
                                                <li><a href="{{route('view.maqola')}}">Ilmiy maqolalar</a></li>
                                                <hr>
                                                <li><a href="">Taniqli siyosatshunos olimlar</a></li>
                                                <hr>
                                                <li><a href="{{route('view.konferensiya')}}">Konferensiya va anjumanlar axborotnoma</a></li>
                                            </ul>
                                </div>
                            </li>
{{--                            <li>--}}
{{--                                <a href="{{ route('talim') }}">Ta`lim</a>--}}
{{--                                <div class="megamenu">--}}
{{--                                    <ul>--}}
{{--                                        <li><a href="{{route('view.tadqiqot')}}">Ilmiy tadqiqot</a></li>--}}
{{--                                        <hr>--}}
{{--                                        <li><a href="{{route('view.maqola')}}">Ilmiy maqolalar</a></li>--}}
{{--                                        <hr>--}}
{{--                                        <li><a href="">Taniqli siyosatshunos olimlar</a></li>--}}
{{--                                        <hr>--}}
{{--                                        <li><a href="{{route('view.konferensiya')}}">Konferensiya va anjumanlar axborotnoma</a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}

                            <li>
                                <a href="#">Anketa va so'rovnoma</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="">Anketa va so'rovnomalar</a></li>
                                                <hr>
                                                <li><a href="">O'tkazilgan so'rov natijalari</a></li>
                                                <hr>
                                                <li><a href="">Analitika</a></li>
                                            </ul>
                                </div>
                            </li>
{{--Akademik faoliyat--}}
                            <li>
                                <a href="#">Akademik faoliyat</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="{{route('view.akademik')}}">Akademik dasturlar</a></li>
                                                <hr>
                                                <li><a href="{{route('view.katalog')}}">Dastur katalogi</a></li>
                                                <hr>
                                                <li><a href="">Bitiruvchilar</a></li>
                                                <hr>
                                                <li><a href="">Amaliyot imkoniyatlari</a></li>
                                                <hr>
                                                <li><a href="">Onlayn ta'lim</a></li>
                                            </ul>
                                </div>
                            </li>
{{--Kutubxona--}}
                            <li>
                                <a href="#">Kutubxona</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="{{route('view.siyosat_kirish')}}">Siyosatshunoslikka kirish</a></li>
                                                <hr>
                                                <li><a href="{{route('view.siyosat_metodika')}}">Siyosatshunoslikni o'qitish metodikasi</a></li>
                                                <hr>
                                                <li><a href="{{route('view.siyosat_fanlar')}}">Boshqa siyosiy fanlar</a></li>
                                                <hr>
                                                <li><a href="{{route('view.savols')}}">100 savolga 100 javob</a></li>
                                                <hr>
                                                <li><a href="{{route('view.kutubxona')}}">Elektron kutubxona</a></li>
                                                <hr>
                                                <li><a href="{{route('view.lugat')}}">Elektron lug'at</a></li>
                                            </ul>
                                </div>
                            </li>
{{--Aloqa va axborot xizmati--}}
                            <li>
                                <a href="#">Aloqa va axborot xizmati</a>
                                <div class="megamenu">
                                            <ul>
                                                <li><a href="{{route('view.yangilik')}}">Yangiliklar</a></li>
                                                <hr>
                                                <li><a href="{{route('view.elon')}}">E'lonlar</a></li>
                                                <hr>
                                                <li><a href="">Axborot xizmati bilan bog'lanish</a></li>
                                                <hr>
                                                <li><a href="{{route('view.fotogalareya')}}">Fotogalareya</a></li>
                                                <hr>
                                                <li><a href="{{route('view.havola')}}">Foydali havolalar</a></li>
                                            </ul>
                                </div>
                            </li>
{{--                            <li>--}}
{{--                                <a href="#">Test</a>--}}
{{--                            </li>--}}

                            <a href="{{route('login')}}" class="link-to-tab d-md-show" style="font-size: 13px"><i class="d-icon-user">&nbsp;</i>Kirish</a>
                        </ul>
                    </nav>
                </div>
                <div class="header-right">
                </div>
            </div>
        </div>
    </div>
</header>
