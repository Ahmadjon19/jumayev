@extends('main.layouts.master')
@section('title', 'Elektron lug\'atlar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Barcha lug'atlar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>So'z</th>
                                <th>Ma'nosi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lugats as $key=>$lugat)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$lugat->suz}}</td>
                                    <td>{{$lugat->mano}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

