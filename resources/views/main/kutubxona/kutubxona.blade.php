@extends('main.layouts.master')
@section('title', 'Kutubxona')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Kitoblar ro'yxati</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kitob nomi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($kutubxonas as $key=>$kutubxona)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><a href="{{asset('admin/kutubxona/'.$kutubxona->file)}}">{{$kutubxona->name}}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

