@extends('main.layouts.master')
@section('title', 'Foydali havolalar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Foydali havolalar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Havola</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($havolas as $key=>$havola)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><a href="{{$havola->havola}}">{{$havola->name}}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

