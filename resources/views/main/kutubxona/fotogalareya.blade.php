@extends('main.layouts.master')
@section('title', 'Fotogalareya')
@section('content')
    @for($i=0; $i<count($fotos); $i++)
    <br>
    <div class="container-fluid">
        <div class="row">
            <div style="justify-content: center; align-items: center; text-align: center" class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$fotos[$i]->name}}</h5>
                    </div>
                    <div class="card-body">
                        <p>{{$fotos[$i]->matn}}</p>
                    </div>
                    <div class="card-body">
                        <div class="row my-gallery gallery" id="aniimated-thumbnials" itemscope="" data-pswp-uid="1">
                            @for($j=0; $j<count($all_image[$i])-1; $j++)
                                <figure class="col-md-3 col-6 img-hover hover-1" itemprop="associatedMedia" itemscope="">
                                    <a href="{{asset('admin/fotogalareya/'.$all_image[$i][$j])}}" itemprop="contentUrl" data-size="1600x950" data-bs-original-title="" title="">
                                        <div><img src="{{asset('admin/fotogalareya/'.$all_image[$i][$j])}}" itemprop="thumbnail" alt="Image description"></div>
                                    </a>
                                    <figcaption itemprop="caption description"></figcaption>
                                </figure>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endfor
@endsection
