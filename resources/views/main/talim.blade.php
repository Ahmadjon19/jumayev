@extends('main.layouts.master')
@section('title', 'Ta`lim')
@section('content')
    <div class="container pt-5">
        <div class="row" >
            <div class="col-xl-4 col-md-6 col-sm-12">
                <section class="mb-3" style=" border-top: 3px solid rgb(34, 102, 204);  background: #f2f2f2;">

                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button style="font-size: 15px" class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Siyosatshunoslik va malaka talablari
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <a style="font-size: 13px; font-weight: 600; letter-spacing: 1px;" href="{{ route('iframe',1) }}">Siyosatshunoslik va malaka talablari</a>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button style="font-size: 15px" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Konfirensiya va anjuman
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <a style="font-size: 13px; font-weight: 600; letter-spacing: 1px;" href="{{ route('iframe',2) }}">Konfirensiya va anjuman</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
            <div class="col-xl-8 col-md-6 col-sm-12">
                <section>
                    <div>
                        <h1 style="font-size: 70px; color: rgb(34, 102, 204);">Amaliy siyosiy tadqiqotlar klinikasi</h1>

                        <p style="font-size: 1.7rem; margin-bottom: 30px; display: block;"> Amaliy siyosiy tadqiqotlar klinikasi - Buxoro davlat universitetining Ijtimoiy-siyosiy fanlar
                            kafedrasi loyihasi bo‘lib, talabalarga nazariy bilimlarni aniq amaliy muammolarni hal qilish
                            ko‘nikmalarini shakllantirish imkonini beradi.
                        </p>
                        <h3 style="font-size: 1.2rem; font-weight: 600; display: block; text-transform: uppercase;">BIZNING VAZIFAMIZ</h3>
                        <h5 style=" font-size: 3.2rem; font-weight: 500; display: block; text-transform: uppercase; margin-bottom: 30px"> Hamkorlik va mijozlar manfaatlarini ko‘zlab loyiha-tahlil ishlarini amalga oshirish orqali
                            siyosatshunos talabalarning talab qilinadigan ko‘nikmalarini, martaba yo‘llarini va kasbiy
                            yo‘nalishini rivojlantirish.
                        </h5>
                        <p style="margin-bottom: 40px; display: block"> Buxoro davlat universiteti qoshidagi Amaliy siyosiy tadqiqotlar klinikasi - bu har bir kishi
                            xato qilishdan qo‘rqmasdan o‘zini haqiqiy amaliy siyosiy loyihalarda sinab ko‘rishi, jamoadan
                            yordam olishi, mutaxassislik bo‘yicha qaror qabul qilishi, o‘z rivojlanishi uchun yangi imkoniyatlarni
                            topishi mumkin bo‘lgan maxsus ta'lim maydoni bo‘lib, bu kelajakda ularga mehnat bozorida ko‘proq
                            izlanuvchi mutaxassis bo‘lishga yordam beradi va yangi kasbiy rivojlanish imkoniyatlarini ochib
                            beradi va faol fuqarolik pozisiyasini shakllantirishga xizmat qiladi.
                        </p>
                        <h1 style="font-size: 3.2rem; font-weight: 500; display: block; text-transform: uppercase; margin-bottom: 30px; color: rgb(34, 102, 204);">Asosiy yo‘nalishlar</h1>
                        Tadqiqot va ishlanmalar

                        Mijozlar manfaatlarini ko‘zlab, individual va jamoaviy amaliy tadqiqot loyihalarini amalga oshirish
                        Tashkiliy

                        Siyosiy kampaniyalarni, shuningdek, turli darajadagi va miqyosdagi ommaviy tadbirlarni o‘tkazish uchun
                        tashkiliy va maslahat xizmatlarini ko‘rsatish, konsalting Tarbiyaviy

                        Siyosatshunoslik boʻyicha ilmiy-ommabop onlayn maʼruzalar, oʻquv podkastlari kabi taʼlim
                        loyihalarini amalga oshirish Karera yo‘nalishi

                        "Bitiruvchi-talaba" avlodlari o‘rtasidagi kasbiy o‘zaro munosabatlar orqali mumkin bo‘lgan martaba
                        yo‘llarini tanlash uchun motivasiya

                        Klinika bakalavriat va magistratura ta'lim dasturlarini qamrab oladi.

                        Bakalavriat dasturlari
                        60310100-siyosatshunoslik ta'lim yo‘nalishi (1-ilova)


                        Klinika loyihalarida siyosatshunos talabalardan tashqari, Buxoro davlat universitetining boshqa
                        ta'lim yo‘nalishlari talabalari, shuningdek, boshqa universitetlar talabalari ham ishtirok etishlari
                        mumkin.
                        86
                        Talabalar Klinika loyihalarida faol ishtirok etadilar.
                        Klinikaning mijozlari:
                        • Buxoro viloyati davlat hokimiyatining ijro etuvchi organlari
                        • Mahalliy hokimiyat organlarining kengashlari.
                        • Jamoat tashkilotlari va siyosiy partiyalarning hududiy bo‘limlari
                        • Nodavlat va notijorat tashkilotlar
                        • Korxona va tashkilotlarning axborot-tahliliy bo‘limlari va xizmatlari

                        Jumaev Rustam G‘anievich - Amaliy siyosiy tadqiqotlar klinikasi rahbari, siyosiy fanlar bo‘yicha
                        falsafa doktori, “Ijtimoiy-siyosiy fanlar” kafedrasi professori, polit@buxdu.uz


                    </div>
                </section>
                <section class="constructor-box constructor-grid">
                    <h3 class="box-title" style="margin-top: 40px; display: block; font-weight: 500; font-size: 30px; color: rgb(34, 102, 204);">
                        Asosiy yo‘nalishlar
                    </h3>

                    <div class="row card-list--xsmall">
                        <div class="col-xs-12 col-md-6 col-xl-3">
                            <div class="card-info  card-info--small" style="border: 1px solid #e9e9e9; padding: 10px 15px; max-height: 450px; height: 220px">
                                <div class="card__media"></div>
                                <div class="card__content ">
                                    <div class="card__crop-content js-crop-text" style="overflow-wrap: break-word;">
                                        <div class="card__header"><h4 class="card__title" style="font-size: 20px;"> Tadqiqot va ishlanmalar </h4></div>
                                        <p class="card__desc" style="font-size: 13px">Mijozlar manfaatlarini ko‘zlab, individual va jamoaviy amaliy tadqiqot loyihalarini amalga oshirish</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-xl-3">
                            <div class="card-info  card-info--small " style="border: 1px solid #e9e9e9; padding: 10px 15px; max-height: 450px; height: 220px">
                                <div class="card__media"></div>
                                <div class="card__content ">
                                    <div class="card__crop-content js-crop-text" style="overflow-wrap: break-word;">
                                        <div class="card__header"><h4 class="card__title" style="font-size: 20px;"> Tashkiliy</h4></div>
                                        <p class="card__desc" style="font-size: 13px;text-align: justify">Siyosiy kampaniyalarni, shuningdek, turli darajadagi va miqyosdagi ommaviy tadbirlarni o‘tkazish uchun tashkiliy va maslahat xizmatlarini ko‘rsatish, konsalting</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-xl-3">
                            <div class="card-info  card-info--small " style="border: 1px solid #e9e9e9; padding: 10px 15px; max-height: 450px; height: 220px">
                                <div class="card__media"></div>
                                <div class="card__content ">
                                    <div class="card__crop-content js-crop-text" style="overflow-wrap: break-word;">
                                        <div class="card__header"><h4 class="card__title" style="font-size: 20px;"> Tarbiyaviy </h4></div>
                                        <p class="card__desc" style="font-size: 13px; text-align: justify">Siyosatshunoslik boʻyicha ilmiy-ommabop onlayn maʼruzalar, oʻquv podkastlari kabi taʼlim loyihalarini amalga oshirish</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-xl-3">
                            <div class="card-info  card-info--small " style="border: 1px solid #e9e9e9; padding: 10px 15px; max-height: 450px; height: 220px">
                                <div class="card__media"></div>
                                <div class="card__content ">
                                    <div class="card__crop-content js-crop-text" style="overflow-wrap: break-word;">
                                        <div class="card__header"><h4 class="card__title" style="font-size: 20px;"> Karera yo‘nalishi</h4></div>
                                        <p class="card__desc" style="font-size: 13px;text-align: justify">"Bitiruvchi-talaba" avlodlari o‘rtasidagi kasbiy o‘zaro munosabatlar orqali mumkin bo‘lgan martaba yo‘llarini tanlash uchun motivasiya</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>Klinika bakalavriat va magistratura ta'lim dasturlarini qamrab oladi.</p>
                    <h3>Bakalavriat dasturlari</h3>

                    <a href="{{ route('ilova') }}" style="display: block; background: #f7f7f5; color: rgb(34, 102, 204); border-left: 4px solid #4a4a4a; border-bottom: 1px solid rgb(34, 102, 204); padding: 10px">60310100-siyosatshunoslik ta'lim yo‘nalishi</a>
                    <p style="margin-top: 20px;text-align: justify">Klinika loyihalarida siyosatshunos talabalardan tashqari, Buxoro davlat universitetining boshqa ta'lim yo‘nalishlari talabalari, shuningdek, boshqa universitetlar talabalari ham ishtirok etishlari mumkin.</p>
                    <h1 style="font-size: 100px; color: rgb(34, 102, 204);">86</h1>
                    <p>Talabalar Klinika loyihalarida faol ishtirok etadilar.</p>
                    <b>Klinikaning mijozlari:</b>
                    <ul>
                        <li>Buxoro viloyati davlat hokimiyatining ijro etuvchi organlari</li>
                        <li>Mahalliy hokimiyat organlarining kengashlari. </li>
                        <li>Jamoat tashkilotlari va siyosiy partiyalarning hududiy bo‘limlari</li>
                        <li>Nodavlat va notijorat tashkilotlar</li>
                    </ul>

                    <img src="{{ asset('1.jpg') }}" width="150px" alt="logo">
                    <p style="margin: 15px 0;">Жумаев Рустам Ғаниевич - Амалий сиёсий тадқиқотлар клиникаси раҳбари, сиёсий фанлар бўйича фалсафа доктори, “Ижтимоий-сиёсий фанлар” кафедраси профессори, <strong style="color:blue;">polit@buxdu.uz</strong>  </p>
                    <img src="{{asset('2.png')}}" style="max-width: 100%;" alt="logo">
                </section>
            </div>
        </div>

    </div>
@endsection
