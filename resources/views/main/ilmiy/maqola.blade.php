@extends('main.layouts.master')
@section('title', 'Ilmiy maqolalar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Ilmiy maqolalar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Maqola nomi</th>
                                <th>Maqola fayl</th>
{{--                                <th>Maqola turi</th>--}}
                                <th>Maqola tavsifi</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($maqolas as $key=>$maqola)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$maqola->name}}</td>
                                    <td><a href="{{asset('admin/'.$maqola->turi.'/'.$maqola->fayl)}}">{{$maqola->fayl}}</a></td>
{{--                                    <td>{{$maqola->turi}}</td>--}}
                                    <td>{{$maqola->izoh}}</td>
{{--                                    <td>{{$maqola->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

