@extends('main.layouts.master')
@section('title', 'Ilmiy tadqiqotlar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Ilmiy tadqiqotlar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tadqiqot nomi</th>
                                <th>Tadqiqot fayl</th>
{{--                                <th>Tadqiqot type</th>--}}
                                <th>Tadqiqot tavsifi</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tadqiqots as $key=>$tadqiqot)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$tadqiqot->name}}</td>
                                    <td><a href="{{asset('admin/'.$tadqiqot->turi.'/'.$tadqiqot->fayl)}}">{{$tadqiqot->fayl}}</a></td>
{{--                                    <td>{{$tadqiqot->turi}}</td>--}}
                                    <td>{{$tadqiqot->izoh}}</td>
{{--                                    <td>{{$tadqiqot->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

