@extends('main.layouts.master')
@section('title', 'Konferensiya va anjumanlar')
@section('content')
    <br><br>

    <div class="container">

        <div class="col-sm-11 container">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5>Konferensiya va anjumanlar</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Konferensiya mavzusi</th>
                                <th>Konferensiya fayli</th>
                                <th>Konferensiya vaqti</th>
{{--                                <th>Created date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($konferensiyas as $key=>$konferensiya)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$konferensiya->mavzusi}}</td>
                                    <td><a href="{{asset('admin/konferensiya/'.$konferensiya->nizom)}}">{{$konferensiya->nizom}}</a></td>
                                    <td>{{$konferensiya->muddat}}</td>
{{--                                    <td>{{$konferensiya->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

